﻿Imports System.Runtime.InteropServices
Imports System
Imports System.Text

''' <summary>
''' NKF32 Class
''' </summary>
''' <remarks></remarks>
Public Class nkf32

#Region "1 ﾊﾞｰｼﾞｮﾝ"

    ''' <summary>
    ''' nkf32.dllのﾊﾞｰｼﾞｮﾝを返す
    ''' </summary>
    ''' <param name="verStr">ByRef nkf32.dllﾊﾞｰｼﾞｮyﾝ</param>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Sub GetNkfVersion(ByVal verStr As StringBuilder)
    End Sub

    ''' <summary>
    ''' nkf32.dllのバージョンを返す
    ''' </summary>
    ''' <param name="verStr">ByRef nkf32.dllﾊﾞｰｼﾞｮyﾝ</param>
    ''' <param name="nBufferLength">ByVal verStrの領域長</param>
    ''' <param name="lpTCHARsReturned">ByRef ﾊﾞｰｼﾞｮyﾝ文字列の長さ</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function GetNkfVersionSafeA(ByVal verStr As StringBuilder, _
                                                                       ByVal nBufferLength As UInt32, _
                                                                       ByRef lpTCHARsReturned As UInt32) As Boolean
    End Function

    '///// Unicodeをｻﾎﾟｰﾄしない為常に失敗 /////
    ' ''' <summary>
    ' ''' nkf32.dllのバージョンを返す
    ' ''' </summary>
    ' ''' <param name="verStr">ByRef nkf32.dllﾊﾞｰｼﾞｮyﾝ</param>
    ' ''' <param name="nBufferLength">ByVal verStrの領域長</param>
    ' ''' <param name="lpTCHARsReturned">ByRef ﾊﾞｰｼﾞｮyﾝ文字列の長さ</param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    '<DllImport("nkf32.dll")> Public Shared Function GetNkfVersionSafeW(ByVal verStr As StringBuilder, _
    '                                                                   ByVal nBufferLength As UInt32, _
    '                                                                   ByRef lpTCHARsReturned As UInt32) As Boolean
    'End Function

    ''' <summary>
    ''' nkf32.dllのバージョンを返す
    ''' </summary>
    ''' <param name="verStr">ByRef nkf32.dllﾊﾞｰｼﾞｮyﾝ</param>
    ''' <param name="nBufferLength">ByVal verStrの領域長</param>
    ''' <param name="lpTCHARsReturned">ByRef ﾊﾞｰｼﾞｮyﾝ文字列の長さ</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function GetNkfVersionSafe(ByVal verStr As StringBuilder, _
                                                                      ByVal nBufferLength As UInt32, _
                                                                      ByRef lpTCHARsReturned As UInt32) As Boolean
    End Function

#End Region

#Region "2 変換ｵﾌﾟｼｮﾝの指定"

    ''' <summary>
    ''' 変換ｵﾌﾟｼｮﾝ文字列を指定(連続したｵﾌﾟｼｮﾝの指定も可能)
    ''' </summary>
    ''' <param name="outStr">ByVal 変換オプション文字列を指定</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function SetNkfOption(ByVal outStr As StringBuilder) As Int32
    End Function

#End Region

#Region "3 コード変換"

    ''' <summary>
    ''' 文字コードを変換する
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="inStr">ByVal 入力文字列</param>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Sub NkfConvert(ByVal outStr As StringBuilder, _
                                                          ByVal inStr As StringBuilder)
    End Sub
    ''' <summary>
    ''' 文字コードを変換する
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="nOutBufferLength">ByVal outStrの領域長(Byte数)</param>
    ''' <param name="lpBytesReturned">ByRef 出力文字列の長さ</param>
    ''' <param name="inStr">ByVal 入力文字列</param>
    ''' <param name="nInBufferLength">ByVal 入力文字列の長さ(Byte数)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function NkfConvertSafe(ByVal outStr As StringBuilder, _
                                                                   ByVal nOutBufferLength As UInt32, _
                                                                   ByRef lpBytesReturned As UInt32, _
                                                                   ByVal inStr As StringBuilder, _
                                                                   ByVal nInBufferLength As UInt32) As Boolean
    End Function

#End Region

#Region "4 全角英数字・記号文字列の半角文字列変換"

    ''' <summary>
    ''' 全角英数字・記号文字列を半角文字列に変換
    ''' </summary>
    ''' <param name="inStr">ByRef 入出力文字列</param>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Sub ToHankaku(ByVal inStr As StringBuilder)
    End Sub

    ''' <summary>
    ''' 全角英数字・記号文字列を半角文字列に変換
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="nOutBufferLength">ByVal outStrの領域長(Byte数)</param>
    ''' <param name="lpBytesReturned">ByRef 出力文字列の長さ</param>
    ''' <param name="inStr">ByVal 入力文字列</param>
    ''' <param name="nInBufferLength">ByVal 入力文字列の長さ(Byte数)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function ToHankakuSafe(ByVal outStr As StringBuilder, _
                                                                  ByVal nOutBufferLength As UInt32, _
                                                                  ByRef lpBytesReturned As UInt32, _
                                                                  ByVal inStr As StringBuilder, _
                                                                  ByVal nInBufferLength As UInt32) As Boolean
    End Function

#End Region

#Region "5 半角ｶﾀｶﾅの全角ｶﾀｶﾅ変換"

    ''' <summary>
    ''' 半角ｶﾀｶﾅと一部の記号（｢｣ｰ等）を全角ｶﾀｶﾅに変換
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="inStr">ByVal 入力文字列</param>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Sub ToZenkakuKana(ByRef outStr As StringBuilder, _
                                                             ByVal inStr As StringBuilder)
    End Sub

    ''' <summary>
    ''' 半角ｶﾀｶﾅと一部の記号（｢｣ｰ等）を全角ｶﾀｶﾅに変換
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="nOutBufferLength">ByVal outStrの領域長(Byte長)</param>
    ''' <param name="lpBytesReturned">ByRef 出力文字列の長さ</param>
    ''' <param name="inStr">ByVal 入力文字列</param>
    ''' <param name="nInBufferLength">ByVal 入力文字列の長さ(Byte長)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function ToZenkakuKanaSafe(ByRef outStr As StringBuilder, _
                                                                      ByVal nOutBufferLength As UInt32, _
                                                                      ByRef lpBytesReturned As UInt32, _
                                                                      ByVal inStr As StringBuilder, _
                                                                      ByVal nInBufferLength As UInt32) As Boolean
    End Function

#End Region

#Region "6 ﾒｰﾙｻﾌﾞｼﾞｪｸﾄのMIMEｴﾝｺｰﾄﾞ"

    ''' <summary>
    ''' ﾒｰﾙｻﾌﾞｼﾞｪｸﾄをMIME(Base64)に変換
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="inStr">ByVal 入力文字列</param>
    ''' <remarks>SJIS変換時はISO-2022-JPに変換した後変換を行う事</remarks>
    <DllImport("nkf32.dll")> Public Shared Sub EncodeSubject(ByVal outStr As StringBuilder, _
                                                             ByVal inStr As StringBuilder)
    End Sub

    ''' <summary>
    ''' ﾒｰﾙｻﾌﾞｼﾞｪｸﾄをMIME(Base64)に変換
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="inStr">ByVal 入力文字列</param>
    ''' <remarks>SJIS変換時はISO-2022-JPに変換した後変換を行う事</remarks>
    <DllImport("nkf32.dll")> Public Shared Sub ToMime(ByVal outStr As StringBuilder, _
                                                      ByVal inStr As StringBuilder)
    End Sub

    ''' <summary>
    ''' ﾒｰﾙｻﾌﾞｼﾞｪｸﾄをMIME(Base64)に変換
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="nOutBufferLength">ByVal outStrの領域長(Byte数)</param>
    ''' <param name="lpBytesReturned">ByRef 出力文字列の長さ</param>
    ''' <param name="inStr">ByVal 入力文字列</param>
    ''' <param name="nInBufferLength">ByVal 入力文字列の長さ(Byte数)</param>
    ''' <returns></returns>
    ''' <remarks>SJIS変換時はISO-2022-JPに変換した後変換を行う事</remarks>
    <DllImport("nkf32.dll")> Public Shared Function EncodeSubjectSafe(ByVal outStr As StringBuilder, _
                                                                      ByVal nOutBufferLength As UInt32, _
                                                                      ByRef lpBytesReturned As UInt32, _
                                                                      ByVal inStr As StringBuilder, _
                                                                      ByVal nInBufferLength As UInt32) As Boolean
    End Function

#End Region

#Region "7 ｺｰﾄﾞ種別の取得"

    ''' <summary>
    ''' 自動判別したｺｰﾄﾞ種別を取得
    ''' </summary>
    ''' <returns>0:シフトJIS/1:EUC/2:ISO-2022-JP/3:UTF-8/4:UTF-16LE/5:UTF-16BE</returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function NkfGetKanjiCode() As Int32
    End Function

    ''' <summary>
    ''' 自動判別したｺｰﾄﾞ種別を取得
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="nBufferLength">ByVal outStrの領域長(Byte数)</param>
    ''' <param name="lpTCHARsReturned">ByRef 出力文字列の長さ</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function GetNkfGuessA(ByVal outStr As StringBuilder, _
                                                                 ByVal nBufferLength As UInt32, _
                                                                 ByRef lpTCHARsReturned As StringBuilder) As Boolean
    End Function

    '///// Unicodeをｻﾎﾟｰﾄしない為常に失敗 /////
    ' ''' <summary>
    ' ''' 自動判別したｺｰﾄﾞ種別を取得
    ' ''' </summary>
    ' ''' <param name="outStr">ByRef 出力文字列</param>
    ' ''' <param name="nBufferLength">ByVal outStrの領域長(Byte数)</param>
    ' ''' <param name="lpTCHARsReturned">ByRef 出力文字列の長さ</param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    '<DllImport("nkf32.dll")> Public Shared Function GetNkfGuessW(ByVal outStr As StringBuilder, _
    '                                                             ByVal nBufferLength As UInt32, _
    '                                                             ByRef lpTCHARsReturned As StringBuilder) As Boolean
    'End Function

    ''' <summary>
    ''' 自動判別したｺｰﾄﾞ種別を取得
    ''' </summary>
    ''' <param name="outStr">ByRef 出力文字列</param>
    ''' <param name="nBufferLength">ByVal outStrの領域長(Byte数)</param>
    ''' <param name="lpTCHARsReturned">ByRef 出力文字列の長さ</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function GetNkfGuess(ByVal outStr As StringBuilder, _
                                                                ByVal nBufferLength As UInt32, _
                                                                ByRef lpTCHARsReturned As StringBuilder) As Boolean
    End Function

#End Region

#Region "8 ﾌｧｲﾙ変換"

    ''' <summary>
    ''' ﾌｧｲﾙをｺｰﾄﾞ変換
    ''' </summary>
    ''' <param name="fName">ByVal 入出力ﾌｧｲﾙ名</param>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Sub NkfFileConvert1(ByVal fName As StringBuilder)
    End Sub

    ''' <summary>
    ''' ﾌｧｲﾙをｺｰﾄﾞ変換
    ''' </summary>
    ''' <param name="fName">ByVal 入出力ﾌｧｲﾙ名</param>
    ''' <param name="nBufferLength">ByVal 入出力ﾌｧｲﾙ名の長さ(文字数)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function NkfFileConvert1SafeA(ByVal fName As StringBuilder, _
                                                                         ByVal nBufferLength As UInt32) As Boolean
    End Function

    '///// Unicodeをｻﾎﾟｰﾄしない為常に失敗 /////
    ' ''' <summary>
    ' ''' ﾌｧｲﾙをｺｰﾄﾞ変換
    ' ''' </summary>
    ' ''' <param name="fName">ByVal 入出力ﾌｧｲﾙ名</param>
    ' ''' <param name="nBufferLength">ByVal 入出力ﾌｧｲﾙ名の長さ(文字数)</param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    '<DllImport("nkf32.dll")> Public Shared Function NkfFileConvert1SafeW(ByVal fName As StringBuilder, _
    '                                                                     ByVal nBufferLength As UInt32) As Boolean
    'End Function

    ''' <summary>
    ''' ﾌｧｲﾙをｺｰﾄﾞ変換
    ''' </summary>
    ''' <param name="fName">ByVal 入出力ﾌｧｲﾙ名</param>
    ''' <param name="nBufferLength">ByVal 入出力ﾌｧｲﾙ名の長さ(文字数)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function NkfFileConvert1Safe(ByVal fName As StringBuilder, _
                                                                        ByVal nBufferLength As UInt32) As Boolean
    End Function

    ''' <summary>
    ''' ﾌｧｲﾙをｺｰﾄﾞ変換
    ''' </summary>
    ''' <param name="fInName">ByVal 入力ﾌｧｲﾙ名</param>
    ''' <param name="fOutName">ByVal 出力ﾌｧｲﾙ名</param>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Sub NkfFileConvert2(ByVal fInName As StringBuilder, _
                                                               ByVal fOutName As StringBuilder)
    End Sub

    ''' <summary>
    ''' ﾌｧｲﾙをｺｰﾄﾞ変換
    ''' </summary>
    ''' <param name="fInName">ByVal 入力ﾌｧｲﾙ名</param>
    ''' <param name="fInBufferLength">ByVal 入力ﾌｧｲﾙ名の長さ(文字数)</param>
    ''' <param name="fOutName">ByVal 出力ﾌｧｲﾙ名</param>
    ''' <param name="fOutBufferLength">ByVal 出力ﾌｧｲﾙ名の長さ(文字数)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function NkfFileConvert2SafeA(ByVal fInName As StringBuilder, _
                                                                         ByVal fInBufferLength As UInt32, _
                                                                         ByVal fOutName As StringBuilder, _
                                                                         ByVal fOutBufferLength As UInt32) As Boolean
    End Function

    '///// Unicodeをｻﾎﾟｰﾄしない為常に失敗 /////
    ' ''' <summary>
    ' ''' ﾌｧｲﾙをｺｰﾄﾞ変換
    ' ''' </summary>
    ' ''' <param name="fInName">ByVal 入力ﾌｧｲﾙ名</param>
    ' ''' <param name="fInBufferLength">ByVal 入力ﾌｧｲﾙ名の長さ(文字数)</param>
    ' ''' <param name="fOutName">ByVal 出力ﾌｧｲﾙ名</param>
    ' ''' <param name="fOutBufferLength">ByVal 出力ﾌｧｲﾙ名の長さ(文字数)</param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    '<DllImport("nkf32.dll")> Public Shared Function NkfFileConvert2SafeW(ByVal fInName As StringBuilder, _
    '                                                                     ByVal fInBufferLength As UInt32, _
    '                                                                     ByVal fOutName As StringBuilder, _
    '                                                                     ByVal fOutBufferLength As UInt32) As Boolean
    'End Function

    ''' <summary>
    ''' ﾌｧｲﾙをｺｰﾄﾞ変換
    ''' </summary>
    ''' <param name="fInName">ByVal 入力ﾌｧｲﾙ名</param>
    ''' <param name="fInBufferLength">ByVal 入力ﾌｧｲﾙ名の長さ(文字数)</param>
    ''' <param name="fOutName">ByVal 出力ﾌｧｲﾙ名</param>
    ''' <param name="fOutBufferLength">ByVal 出力ﾌｧｲﾙ名の長さ(文字数)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("nkf32.dll")> Public Shared Function NkfFileConvert2Safe(ByVal fInName As StringBuilder, _
                                                                        ByVal fInBufferLength As UInt32, _
                                                                        ByVal fOutName As StringBuilder, _
                                                                        ByVal fOutBufferLength As UInt32) As Boolean
    End Function

#End Region

#Region "9 機能の取得"

    ' ''' <summary>
    ' ''' 著作権、ﾊﾞｰｼﾞｮﾝ、他のnkf32.dllとの互換性を調べる
    ' ''' </summary>
    ' ''' <param name="outStr">ByRef NkfSupportFunctions 構造体</param>
    ' ''' <param name="nBufferLength">ByVal outStrの領域長(Byte数)</param>
    ' ''' <param name="lpBytesReturned">ByRef 構造体の長さ(Byte数)</param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    '<DllImport("nkf32.dll")> Public Shared Function GetNkfSupportFunctions(ByRef outStr As NkfSupportFunctions, _
    '                                                                       ByVal nBufferLength As UInt32, _
    '                                                                       ByRef lpBytesReturned As UInt32) As Boolean
    'End Function

    '<StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    'Public Structure NkfSupportFunctions
    '    Public size As UInt32
    '    Public copyrightA As String
    '    Public versionA As String
    '    Public dateA As String
    '    Public functions As UInt32
    'End Structure

#End Region

#Region "10 Usageの取得"

    ''' <summary>
    ''' nkf -vで表示する文字列を取得
    ''' </summary>
    ''' <param name="outStr">ByVal Usage</param>
    ''' <param name="nBufferLength">ByVal outStrの領域長(Byte数)</param>
    ''' <param name="lpBytesReturned">ByRef 出力文字列の長さ(Byte数)</param>
    ''' <returns></returns>
    ''' <remarks>領域が不足する場合、関数は失敗し必要な領域長は取得できません</remarks>
    <DllImport("nkf32.dll")> Public Shared Function NkfUsage(ByVal outStr As StringBuilder, _
                                                             ByVal nBufferLength As UInt32, _
                                                             ByRef lpBytesReturned As UInt32) As Boolean
    End Function

#End Region

End Class
