﻿Public Class clsUser
    Public USER_ANO As Integer
    Public USER_ID As String = vbNullString
    Public USER_PASS As String = vbNullString
    Public USER_NAME As String = vbNullString
    Public REP_USER_AUTH As String = vbNullString
End Class

Public Class clsOrder
    Public ORDER_ANO As Integer
    Public UPD_STATE As String
    Public ORDER_STATE As String
    Public ORDER_NUMBER As String
    Public DOKUEI_FLG As String
    Public EXCUTE_AGE As String
    Public NAIGAI_DIV As String
    Public DEPT As String
    Public ORDER_DOC As String
    Public ORDER_WARD As String
    Public NOW_WARD As String
    Public ORDER_DATE As String
    Public ORDER_TIME As String
    Public EXCUTE_DATE As String
    Public EXCUTE_TIME As String
    Public MODALITY As String
    Public STUDY_NAME As String
    Public BODY_PART As String
    Public MWM_STATE As String
    Public STUDY_INSTANCE_UID As String
    Public INS_DATE As String
    Public INS_TIME As String
    Public UPD_DATE As String
    Public UPD_TIME As String
    '///患者属性
    Public PATIENT_ANO As Integer
    Public PATIENT_ID As String
    Public PATIENT_KANJI As String
    Public PATIENT_EIJI As String
    Public PATIENT_HALF_KANA As String
    Public PATIENT_FULL_KANA As String
    Public PATIENT_BIRTH As String
    Public PATIENT_SEX As String

End Class

'Public Class clsStudyInstanceUID
'    Public CODE_ISO As String
'    Public CODE_ISO_AFFIATION As String
'    Public CODE_ISO_COUNTRY As String
'    Public CODE_ORGANIZATION As String
'    Public CODE_PRODUCT As String
'    Public CODE_SYSTEM As String
'    Public CODE_HOSPTAL As String
'    Public CODE_SERIES As String
'    Public CODE_SYSDATE As String
'    Public CODE_SEQ As String
'End Class

