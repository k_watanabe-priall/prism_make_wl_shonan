﻿'///モダリティマスタ用
Public Class clsMstModality
    Public MODALITY_NO As Integer = 0                       'モダリティ番号
    Public MODALITY_CD As String = vbNullString             'モダリティコード
    Public SCP_AE As String = vbNullString                  'SCP AE
    Public SCP_IP As String = vbNullString                  'SCP IP
    Public SCP_PORT As String = vbNullString                'SCP PORT
    Public SCU_AE As String = vbNullString                  'SCU AE
    Public SCU_IP As String = vbNullString                  'SCU IP
    Public SCU_PORT As String = vbNullString                'SCU PORT
    Public CHARCTERSET As String = vbNullString             'CHARCTERSET
    Public HOME_PATH As String = vbNullString                 'MWM Path
    Public TERMINAL As Integer = 0                          '端末名
End Class

'///オーダ情報テーブル
Public Class clsOrderInf
    Public ORDER_ANO As Long                                'オーダ登録番号
    Public ORDER_STUDY_INSTANCE_UID As String               'オーダ取得時StudyInstanceUID
    Public MWM_STUDY_INSTANCE_UID As String                 'MWM時StudyInstanceUID
    Public ORDER_NO As String                               'オーダ番号
    Public ORDER_DATE As String                             '検査予定日
    Public ORDER_TIME As String                             '検査予定時刻
    Public STUDY_DATE As String                             '検査実施日
    Public STUDY_TIME As String                             '検査実施時刻
    Public ORDER_DIV As String                              'オーダ形態区分(0=RISから取得、1=手入力、2=カードリーダ)
    Public PATIENT_ANO As Long                              '患者登録番号
    Public PATIENT_ID As String                             '患者ID
    Public BODY_PART As String                              '部位(※複数部位の場合カンマ区切り)
    Public STATE As String                                  '進捗(0=未実施、1=受付済、2=実施済、10=中止)
    Public ORDER_COMMENT As String                          'オーダコメント
    Public STUDY_COMMENT As String                          '検査コメント
    Public MODALITY As String                               'モダリティ
    Public MODALITY_NO As Integer                           'モダリティ区分(1=320列、2=64列)
    Public TERMINAL As String                               '端末名
    Public SCP_AE As String
    Public SCU_AE As String = vbNullString
    Public HOME_PATH As String
    Public PATIENT_NAME As String                           '患者氏名
    Public PATIENT_NAME_EIJI As String                      '患者英字氏名
    Public PATIENT_NAME_KANJI As String                     '患者漢字氏名
    Public PATIENT_NAME_KANA As String                      '患者カナ氏名
    Public PATIENT_SEX As String                            '患者性別(1=男性、2=女性、9=不明)
    Public PATIENT_BIRTH As String                          '生年月日
    'Public PATIENT_AGE As String                            '年齢
    Public PATIENT_COMMENT As String                        '患者コメント
    Public CHARCTERSET As String
End Class

'///患者情報テーブル
Public Class clsPatient
    Public PATIENT_ANO As Long                              '患者登録番号
    Public PATIENT_ID As String                             '患者ID
    Public PATIENT_NAME As String                           '患者氏名
    Public PATIENT_NAME_EIJI As String                      '患者英字氏名
    Public PATIENT_NAME_KANJI As String                     '患者漢字氏名
    Public PATIENT_NAME_KANA As String                      '患者カナ氏名
    Public PATIENT_SEX As String                            '患者性別(1=男性、2=女性、9=不明)
    Public PATIENT_BIRTH As String                          '生年月日
    Public PATIENT_AGE As String                            '年齢
    Public NYUUGAI As String                                '入外区分(0=外来、1=入院、2=健診)
    Public WARD As String                                   '病棟
    Public BED_ROOM As String                               '病室
    Public COMMENT As String                                'コメント
    Public PATIENT_COMMENT As String                        '患者コメント
End Class

'///取得TAG
Public Class clsGetOrderTAG
    Public MODALITY_NO As String                            'CT識別CD
    Public SORT_KEY As Integer                              '表示順
    Public TAG As String                                    'TAG
    Public TAG_NAME As String                               'TAG名
    Public TAG_NAME_JP As String                            'TAG和名
    Public REMARKS As String                                '備考
    Public VALUE As String                                  '取得データ退避
End Class

'///UID退避用
Public Class clsUID
    Public ISO_CD As String
    Public ISOAffiliation_CD As String
    Public ISO_Country_CD As String
    Public Organization_CD As String
    Public Product_CD As String
    Public System_CD As String
    Public Hosptal_CD As String
    Public Series_CD As String
End Class

Public Class clsStudyInstanceUID
    Public CODE_ISO As String
    Public CODE_ISO_AFFIATION As String
    Public CODE_ISO_COUNTRY As String
    Public CODE_ORGANIZATION As String
    Public CODE_PRODUCT As String
    Public CODE_SYSTEM As String
    Public CODE_HOSPTAL As String
    Public CODE_SERIES As String
    Public CODE_SYSDATE As String
    Public CODE_SEQ As String
End Class

Public Class TYPE_TRAN_ORDER
    Public ORDER_ANO As String
    Public MWM_STATE As String
    Public PATIENT_ID As String
    Public PATIENT_NAME_KANJI As String
    Public PATIENT_NAME_KANA As String
    Public PATIENT_NAME_EIJI As String
    Public PATIENT_SEX As String
    Public PATIENT_BIRTH As String
    '---START Ver.1.7.0.0 CT引渡のため、身長、体重を追加 ADD By Watanabe 2021.10.15
    Public PATIENT_SHINCHO As String
    Public PATIENT_TAIJYU As String
    '---END Ver.1.7.0.0 CT引渡のため、身長、体重を追加 ADD By Watanabe 2021.10.15
    Public PATIENT_ANO As String
    Public MWM_ORDER_NO As String
    Public ORDER_DATE As String
    Public ORDER_TIME As String
    Public SCU_AE As String
    Public MODALITY_CD As String
    Public HOME_PATH As String
    Public CHARCTERSET As String
    Public MWM_STUDY_INSTANCE_UID As String
    Public TERMINAL As String
    Public PORTABLE As String
    '---施設健診フラグ ADD By Watanabe 2022.11.18
    Public SITEHELTH As String
    '---Ver.1.7.1 Study Instance UIDに付与するため追加 ADD By Watanabe 2022.04.15
    Public MODALITY_NO As String
End Class