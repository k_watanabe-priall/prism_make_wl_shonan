Imports System.Windows.Forms
Imports System.Drawing
Imports System
Imports System.Runtime.InteropServices

Public Class AutoResizer

#Region "構造体定義"
    '********************************
    '* コントロール情報退避用構造体 *
    '********************************
    Private Structure TYPE_INF
        Dim positions As Point          '座標
        Dim sizes As Size               'サイズ
        Dim fonts As Font               'フォント
    End Structure

    '****************************************************************************************
    '* フォーム上コンテナがあるとコンテナ内のコントロールを認識しないネストにて構造体を定義 *
    '* 5階層まで対応                                                                        *
    '****************************************************************************************
    Private Structure TYPE_4
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=50)> _
        Dim typctrl() As TYPE_INF
        Dim typInf As TYPE_INF
    End Structure

    Private Structure TYPE_3
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=50)> _
        Dim typctrl() As TYPE_4
        Dim typInf As TYPE_INF
    End Structure

    Private Structure TYPE_2
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=50)> _
        Dim typctrl() As TYPE_3
        Dim typInf As TYPE_INF
    End Structure

    Private Structure TYPE_1
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=50)> _
        Dim typctrl() As TYPE_2
        Dim typInf As TYPE_INF
    End Structure
#End Region

#Region "変数定義"
    Private initialFormSize As Size     'フォームサイズ退避用

    '******************************
    '* フォーム上のコントロール用 *
    '******************************
    Private typ1() As TYPE_1

    '************
    '* フォーム *
    '************
    Private m_form As Form
#End Region

#Region "New 初期処理"
    Public Sub New(Optional ByRef destinationForm As Form = Nothing)
        If destinationForm IsNot Nothing Then
            m_form = destinationForm

            subInitMetric()
        Else
            m_form = New Form
        End If
    End Sub
#End Region

#Region "subInitMetric フォームと全コントロールの初期情報退避"
    '*****************************************************************
    '* 関数名　　：subInitMetric
    '* 機能概要　：フォームと全コントロールの初期情報退避
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2010/07/29
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '*****************************************************************
    Public Sub subInitMetric()
        Dim intCounter As Integer
        Dim intCounter2 As Integer
        Dim intMajic As Integer = 50

        ReDim typ1(m_form.Controls.Count - 1)

        initialFormSize = New Size(m_form.Width, m_form.Height)

        clpScaleWidth = m_form.Size.Width
        clpScaleHeight = m_form.Size.Height

        '****************************
        '* フォーム上のコントロール *
        '****************************
        For intCounter = 0 To m_form.Controls.Count - 1

            With typ1(intCounter)
                .typInf.positions = New Point(m_form.Controls(intCounter).Location.X, m_form.Controls(intCounter).Location.Y)
                .typInf.sizes = New Size(m_form.Controls(intCounter).Size.Width, m_form.Controls(intCounter).Height)
                .typInf.fonts = New Font(m_form.Controls(intCounter).Font.Name, m_form.Controls(intCounter).Font.Size, m_form.Controls(intCounter).Font.Style, GraphicsUnit.Point)

                If m_form.Controls(intCounter).Controls.Count > 0 Then
                    ReDim .typctrl(m_form.Controls(intCounter).Controls.Count - 1)

                    If TypeOf m_form.Controls(intCounter) Is TabPage Then
                        intMajic = 0
                    Else
                        intMajic = 0
                    End If

                    '///PictureBox間延び防止の為 ADD By Watanabe 2011/04/06
                    If TypeOf m_form.Controls(intCounter) Is PictureBox Then
                        Exit Sub
                    End If

                    '****************************
                    '* コンテナ上のコントロール *
                    '****************************
                    For intCounter2 = 0 To m_form.Controls(intCounter).Controls.Count - 1
                        With .typctrl(intCounter2)

                            .typInf.positions = New Point(m_form.Controls(intCounter).Controls(intCounter2).Location.X + intMajic, _
                                                                m_form.Controls(intCounter).Controls(intCounter2).Location.Y)
                            .typInf.sizes = New Size(m_form.Controls(intCounter).Controls(intCounter2).Size.Width, _
                                                           m_form.Controls(intCounter).Controls(intCounter2).Height)
                            .typInf.fonts = New Font(m_form.Controls(intCounter).Controls(intCounter2).Font.Name, _
                                                           m_form.Controls(intCounter).Controls(intCounter2).Font.Size, _
                                                           m_form.Controls(intCounter).Controls(intCounter2).Font.Style, GraphicsUnit.Point)

                            If m_form.Controls(intCounter).Controls(intCounter2).Controls.Count > 0 Then
                                If TypeOf m_form.Controls(intCounter).Controls(intCounter2) Is TabPage Then
                                    intMajic = 0
                                Else
                                    intMajic = 0
                                End If

                                '///PictureBox間延び防止の為 ADD By Watanabe 2011/04/06
                                If TypeOf m_form.Controls(intCounter).Controls(intCounter2) Is PictureBox Then
                                    Exit Sub
                                End If

                                ReDim .typctrl(m_form.Controls(intCounter).Controls(intCounter2).Controls.Count - 1)

                                '****************************
                                '* コンテナ上のコントロール *
                                '****************************
                                For intCounter3 = 0 To m_form.Controls(intCounter).Controls(intCounter2).Controls.Count - 1
                                    With .typctrl(intCounter3)
                                        .typInf.positions = New Point(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Location.X + intMajic, _
                                                                            m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Location.Y)
                                        .typInf.sizes = New Size(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Size.Width, _
                                                                       m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Height)
                                        .typInf.fonts = New Font(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Font.Name, _
                                                                       m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Font.Size, _
                                                                       m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Font.Style, _
                                                                       GraphicsUnit.Point)

                                        '****************************
                                        '* コンテナ上のコントロール *
                                        '****************************
                                        If m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls.Count > 0 Then
                                            If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3) Is TabPage Then
                                                intMajic = 0
                                            Else
                                                intMajic = 0
                                            End If

                                            '///PictureBox間延び防止の為 ADD By Watanabe 2011/04/06
                                            If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3) Is PictureBox Then
                                                Exit Sub
                                            End If

                                            ReDim .typctrl(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls.Count - 1)

                                            For intCounter4 = 0 To m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls.Count - 1
                                                With .typctrl(intCounter4)
                                                    .typInf.positions = New Point(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Location.X + intMajic, _
                                                                                        m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Location.Y)
                                                    .typInf.sizes = New Size(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Size.Width, _
                                                                                   m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Height)
                                                    .typInf.fonts = New Font(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Font.Name, _
                                                                                   m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Font.Size, _
                                                                                   m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Font.Style, _
                                                                                   GraphicsUnit.Point)

                                                    If m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls.Count > 0 Then
                                                        If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4) Is TabPage Then
                                                            intMajic = 0
                                                        Else
                                                            intMajic = 0
                                                        End If

                                                        '///PictureBox間延び防止の為 ADD By Watanabe 2011/04/06
                                                        If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4) Is PictureBox Then
                                                            Exit Sub
                                                        End If

                                                        ReDim .typctrl(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls.Count - 1)

                                                        '****************************
                                                        '* コンテナ上のコントロール *
                                                        '****************************
                                                        For intCounter5 = 0 To m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls.Count - 1
                                                            With .typctrl(intCounter5)
                                                                .positions = New Point(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Location.X + intMajic, _
                                                                                                    m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Location.Y)
                                                                .sizes = New Size(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Size.Width, _
                                                                                               m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Height)
                                                                .fonts = New Font(m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Font.Name, _
                                                                                               m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Font.Size, _
                                                                                               m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Font.Style, _
                                                                                               GraphicsUnit.Point)
                                                            End With
                                                        Next intCounter5

                                                    End If
                                                End With
                                            Next intCounter4

                                        End If
                                    End With
                                Next intCounter3
                            End If
                        End With
                    Next intCounter2

                End If
            End With
        Next intCounter
    End Sub
#End Region

#Region "subResize フォームと全コントロールのリサイズ"
    '*****************************************************************
    '* 関数名　　：subResize
    '* 機能概要　：フォームと全コントロールのリサイズ
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2010/07/29
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '*****************************************************************
    Public Sub subResize()
        Dim widthRatio As Double        '画面幅退避エリア
        Dim heightRatio As Double       '画面高退避エリア
        Dim intCounter As Integer       '1階層目のカウンタ
        Dim intCounter2 As Integer      '2階層目のカウンタ
        Dim intCounter3 As Integer      '3階層目のカウンタ
        Dim intCounter4 As Integer      '4階層目のカウンタ
        Dim intCounter5 As Integer      '5階層目のカウンタ
        Dim intMajic As Integer         '実質未使用

        '///FromSize
        widthRatio = m_form.Width / initialFormSize.Width
        heightRatio = m_form.Height / initialFormSize.Height

        '///画面内のコントロールのリサイズ(コンテナ内のコントロールについて5階層まで対応)
        For intCounter = 0 To m_form.Controls.Count - 1     '1階層目

            With typ1(intCounter)
                If TypeOf m_form.Controls(intCounter) Is Form Then Exit For

                m_form.Controls(intCounter).Size = New Size(CInt(.typInf.sizes.Width * widthRatio), CInt(.typInf.sizes.Height * heightRatio))
                m_form.Controls(intCounter).Location = New Point(CInt(.typInf.positions.X * widthRatio), CInt(.typInf.positions.Y * heightRatio))
                m_form.Controls(intCounter).Font = New Font(G_FONT, .typInf.fonts.Size * widthRatio, .typInf.fonts.Style, GraphicsUnit.Point)

                '///コントロール内のコントロール有無
                If m_form.Controls(intCounter).Controls.Count > 0 Then
                    If TypeOf m_form.Controls(intCounter) Is TabPage Then
                        intMajic = 0
                    Else
                        intMajic = 0
                    End If
                    For intCounter2 = 0 To typ1(intCounter).typctrl.Length - 1  '2階層目
                        With .typctrl(intCounter2)
                            '///コントロールがFormの場合処理を抜ける ADD By Watanabe 2010/11/17
                            If TypeOf m_form.Controls(intCounter).Controls(intCounter2) Is Form Then Exit For

                            m_form.Controls(intCounter).Controls(intCounter2).Size = New Size(CInt(.typInf.sizes.Width * widthRatio), CInt(.typInf.sizes.Height * heightRatio))
                            m_form.Controls(intCounter).Controls(intCounter2).Location = New Point(CInt(.typInf.positions.X * widthRatio - intMajic), CInt(.typInf.positions.Y * heightRatio))
                            m_form.Controls(intCounter).Controls(intCounter2).Font = New Font(G_FONT, .typInf.fonts.Size * widthRatio, .typInf.fonts.Style, GraphicsUnit.Point)

                            '///コントロール内のコントロール有無
                            If m_form.Controls(intCounter).Controls(intCounter2).Controls.Count > 0 Then
                                If TypeOf m_form.Controls(intCounter).Controls(intCounter2) Is TabPage Then
                                    intMajic = 0
                                Else
                                    intMajic = 0
                                End If
                                For intCounter3 = 0 To typ1(intCounter).typctrl(intCounter2).typctrl.Length - 1     '3階層目
                                    With .typctrl(intCounter3)
                                        '///コントロールがFormの場合処理を抜ける ADD By Watanabe 2010/11/17
                                        If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3) Is Form Then Exit For

                                        m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Size = New Size(CInt(.typInf.sizes.Width * widthRatio), CInt(.typInf.sizes.Height * heightRatio))
                                        m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Location = New Point(CInt(.typInf.positions.X * widthRatio - intMajic), CInt(.typInf.positions.Y * heightRatio))
                                        m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Font = New Font(G_FONT, .typInf.fonts.Size * widthRatio, .typInf.fonts.Style, GraphicsUnit.Point)

                                        '///コントロール内のコントロール有無
                                        If m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls.Count > 0 Then
                                            If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3) Is TabPage Then
                                                intMajic = 0
                                            Else
                                                intMajic = 0
                                            End If
                                            For intCounter4 = 0 To typ1(intCounter).typctrl(intCounter2).typctrl(intCounter3).typctrl.Length - 1    '4階層目
                                                With .typctrl(intCounter4)
                                                    '///コントロールがFormの場合処理を抜ける ADD By Watanabe 2010/11/17
                                                    If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4) Is Form Then Exit For

                                                    m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Size = New Size(CInt(.typInf.sizes.Width * widthRatio), CInt(.typInf.sizes.Height * heightRatio))
                                                    m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Location = New Point(CInt(.typInf.positions.X * widthRatio - intMajic), CInt(.typInf.positions.Y * heightRatio))
                                                    m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Font = New Font(G_FONT, .typInf.fonts.Size * widthRatio, .typInf.fonts.Style, GraphicsUnit.Point)

                                                    '///コントロール内のコントロール有無
                                                    If m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls.Count > 0 Then
                                                        If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4) Is TabPage Then
                                                            intMajic = 0
                                                        Else
                                                            intMajic = 0
                                                        End If
                                                        For intCounter5 = 0 To typ1(intCounter).typctrl(intCounter2).typctrl(intCounter3).typctrl(intCounter4).typctrl.Length - 1   '5階層目
                                                            With .typctrl(intCounter5)
                                                                '///コントロールがFormの場合処理を抜ける ADD By Watanabe 2010/11/17
                                                                If TypeOf m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5) Is Form Then Exit For

                                                                m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Size = New Size(CInt(.sizes.Width * widthRatio), CInt(.sizes.Height * heightRatio))
                                                                m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Location = New Point(CInt(.positions.X * widthRatio - intMajic), CInt(.positions.Y * heightRatio))
                                                                m_form.Controls(intCounter).Controls(intCounter2).Controls(intCounter3).Controls(intCounter4).Controls(intCounter5).Font = New Font(G_FONT, .fonts.Size * widthRatio, .fonts.Style, GraphicsUnit.Point)
                                                            End With
                                                        Next intCounter5
                                                    End If
                                                End With
                                            Next intCounter4
                                        End If
                                    End With
                                Next intCounter3
                            End If
                        End With
                    Next intCounter2
                End If
            End With
        Next intCounter

    End Sub
#End Region

End Class
