﻿Imports System.ComponentModel

Public Class frmMain

#Region "定義"
    Private typOrder() As TYPE_TRAN_ORDER
    Private typPatient() As clsPatient
    Private typModality As New clsMstModality
    Private typUID As New clsUID
    Private strTerminal As String = vbNullString
    Private typStudyUID As New clsStudyInstanceUID
    Private aryModality() As clsMstModality
#End Region

#Region "FormEvents"

#Region "FormLoad"

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strMsg As String = vbNullString

        Select Case My.Settings.STUDY_DIV
            Case "0"
                Me.Text &= "(放射線)"
            Case "1"
                Me.Text &= "(超音波)"
            Case "2"
                Me.Text &= "(内視鏡)"
        End Select

        If My.Settings.MAIN_SV_DIV = True Then
            Me.Text &= "(メインサーバ) " & My.Settings.APP_VERSION
        Else
            Me.Text &= "(待機サーバ) " & My.Settings.APP_VERSION
        End If        '///メッセージリスト表示
        Call subDispMsg(0, "!!! PRISM_MAKE_WL START !!!", strMsg)

        '///画面初期処理
        Call subInitForm()

        '---Ver.1.7.0.0 フォームの表示位置/サイズの指定 ADD By Watanabe 2021.10.15
        Me.Left = My.Settings.Left
        Me.Top = My.Settings.Top
        Me.Height = My.Settings.Height
        Me.Width = My.Settings.Width

        Call subOutLog(Me.Text & " Start", 0)
        Call subDispMsg(0, Me.Text & " Start", strMsg)
    End Sub
#End Region

#Region "FormClosing"
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim strMsg As String = vbNullString

        Select Case e.CloseReason
            Case CloseReason.ApplicationExitCall
                strMsg = "Application.Exitによる"
                Console.WriteLine("Application.Exitによる")
            Case CloseReason.FormOwnerClosing
                strMsg = "所有側のフォームが閉じられようとしている"
                Console.WriteLine("所有側のフォームが閉じられようとしている")
            Case CloseReason.MdiFormClosing
                strMsg = "MDIの親フォームが閉じられようとしている"
                Console.WriteLine("MDIの親フォームが閉じられようとしている")
            Case CloseReason.TaskManagerClosing
                strMsg = "タスクマネージャによる"
                Console.WriteLine("タスクマネージャによる")
            Case CloseReason.UserClosing
                strMsg = "ユーザーインターフェイスによる"
                Console.WriteLine("ユーザーインターフェイスによる")
            Case CloseReason.WindowsShutDown
                strMsg = "OSのシャットダウンによる"
                Console.WriteLine("OSのシャットダウンによる")
            Case CloseReason.None
                strMsg = "未知の理由"
                Console.WriteLine("未知の理由")
            Case Else
                strMsg = "それ以外"
                Console.WriteLine("それ以外")
        End Select

        Call subOutLog(strMsg, 0)
    End Sub
#End Region

#End Region

#Region "ControlEvents"

#Region "Timer起動"
    Private Sub tmrProc_Tick(sender As Object, e As System.EventArgs) Handles tmrProc.Tick

        '///Timerを不活性化
        Me.tmrProc.Enabled = False

        '///メイン処理
        Call subMainProc()

        '///Timerを活性化
        Me.tmrProc.Enabled = True

    End Sub
#End Region

#Region "閉じるボタン押下"
    Private Sub cmdClose_Click(sender As System.Object, e As System.EventArgs) Handles cmdClose.Click
        If MsgBox("本アプリケーションを終了すると各モダリティへ引渡す情報が生成されません。" & vbCrLf & "終了しますか。", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, "WLファイル作成") = MsgBoxResult.No Then
            Exit Sub
        End If

        '---START Ver.1.7.0.0 画面の表示位置/サイズの保存 ADD By Watanabe 2021.10.15
        My.Settings.Left = Me.Left
        My.Settings.Top = Me.Top
        My.Settings.Height = Me.Height
        My.Settings.Width = Me.Width
        My.Settings.Save()

        End
    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "画面初期処理(subInitForm)"
    '*******************************************************************
    '* 機能概要　：画面初期処理
    '* 関数名称　：subInitForm
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subInitForm()
        Me.lstMsg.Items.Clear()         'リストボックスのクリア
        tmrProc.Interval = 3000        'TimerIntervalの設定
        tmrProc.Enabled = True          'Timerの活性化

    End Sub
#End Region

#Region "メイン処理(subMainProc)"
    '*******************************************************************
    '* 機能概要　：メイン処理
    '* 関数名称　：subMainProc
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subMainProc()
        Dim strMsg As String = vbNullString
        Dim strMsg1 As String = vbNullString

        Try

            ''///メッセージリスト表示
            ''Call subDispMsg(0, "!!! メイン処理 START !!!", strMsg)
            'Call subOutLog("!!! メイン処理 START !!!", 0)

            '///DataBase接続
            If fncDBConnect(strMsg) = False Then
                Call subDispMsg(1, "DataBaseの接続に失敗しました。(" & strMsg & ")", strMsg1)
                Call subOutLog("DataBaseの接続に失敗しました。(" & strMsg & ")", 1)
                Exit Sub
            End If

            '///WLファイル作成対象データ検索
            Call subGetOrder(strMsg)

            Dim lngCounter As Long = 0
            Dim lngModality As Long = 0

            If typOrder Is Nothing Then Exit Sub

            For lngCounter = 0 To typOrder.Length - 1
                '対象オーダのモダリティ情報取得
                Call subGetModalityInfo(typOrder(lngCounter))

                For lngModality = 0 To aryModality.Length - 1

                    With typOrder(lngCounter)

                        .HOME_PATH = aryModality(lngModality).HOME_PATH
                        .SCU_AE = aryModality(lngModality).SCU_AE
                        .CHARCTERSET = aryModality(lngModality).CHARCTERSET
                        '---Ver.1.7.1 Study Instance UIDに付与するため追加 ADD By Watanabe 2022.04.15
                        .MODALITY_NO = aryModality(lngModality).MODALITY_NO

                        Select Case .MODALITY_CD
                            Case "CR"
                                '///WLファイル作成
                                Call subMakeTempFileCR(typOrder(lngCounter))
                            'Call subDispMsg(0, "CR PushBack START", strMsg)
                            'Call Shell(My.Settings.PUSH_BACK, AppWinStyle.MinimizedNoFocus, True, 6000)
                            'Call subDispMsg(0, "CR PushBack END", strMsg)

                            Case "CT"
                                '///WLファイル作成
                                Call subMakeTempFileCT(typOrder(lngCounter))
                            Case "MR"
                                '///WLファイル作成
                                Call subMakeTempFileMR(typOrder(lngCounter))
                            Case "XA"
                                '///WLファイル作成
                                Call subMakeTempFileXA(typOrder(lngCounter))
                            Case "US"
                                '///WLファイル作成
                                Call subMakeTempFileUS(typOrder(lngCounter))
                            Case "RF"
                                '///WLファイル作成
                                Call subMakeTempFileRF(typOrder(lngCounter))
                            Case "ES"
                                '///WLファイル作成
                                Call subMakeTempFileES(typOrder(lngCounter))
                        End Select

                        '///オーダ情報テーブル更新
                        Call subUpdOrder(typOrder(lngCounter), strMsg)
                    End With
                Next lngModality
            Next lngCounter

        Catch ex As Exception
            strMsg = ex.Message
        Finally
            Call fncDBDisConnect(strMsg)
        End Try

    End Sub
#End Region

#Region " 処理状況を画面に表示(subDispMsg)"
    '**********************************************************************************
    '* 機能概要　：処理状況を画面に表示
    '* 関数名称　：subDispMsg
    '* 引　数　　：intProc 　=　0・・・情報、1=エラー、2=ワーニング
    '* 　　　　　：strOutMsg =表示メッセージ
    '* 　　　　　：strMsg    =エラー内容退避用
    '* 作成日　　：2011/02/04
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subDispMsg(ByVal intProc As Integer, ByVal strOutMsg As String, ByRef strMsg As String)
        Dim strWork As String = vbNullString

        Select Case intProc
            Case 0          '情報
                strWork = "[INF]"
            Case 1          'エラー情報
                strWork = "[ERR]"
            Case 2          'ワーニング情報
                strWork = "[WAR]"
        End Select

        strWork = "( " & Date.Now.ToString & ")" & strWork

        If Me.lstMsg.Items.Count > 256 Then
            Me.lstMsg.Items.Clear()
        End If

        Me.lstMsg.Items.Add(strWork & Space(3) & strOutMsg)
        Me.lstMsg.SelectedIndex = Me.lstMsg.Items.Count - 1
        Me.lstMsg.Refresh()
        Me.Refresh()
    End Sub
#End Region

#Region "WL作成対象データ検索(subGetOrder)"
    '*******************************************************************
    '* 機能概要　：WL作成対象データ検索
    '* 関数名称　：subGetOrder
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subGetOrder(ByRef strMsg As String)
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long
        Dim dsModality As New DataSet
        Dim strPatientID As String = vbNullString
        Dim dcmlWork As Decimal = 0

        Try
            'Call subDispMsg(0, "WL作成対象データ検索", strMsg)
            'Call subOutLog("WL作成対象データ検索", 0)

            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT DISTINCT ")
            strSQL.AppendLine(" ORDER_ANO,")
            strSQL.AppendLine(" MWM_ORDER_NO,")
            strSQL.AppendLine(" MWM_STATE,")
            strSQL.AppendLine(" CONVERT(varchar,JISSHIYOTEI_KAISHI_DATETIME,112) AS ORDER_DATE,")
            strSQL.AppendLine(" REPLACE(CONVERT(varchar,JISSHIYOTEI_KAISHI_DATETIME,108),':','') AS ORDER_TIME,")
            strSQL.AppendLine(" PATIENT_ANO,")
            strSQL.AppendLine(" PATIENT_ID,")
            strSQL.AppendLine(" ISNULL(EIJI_NAME,'') AS PATIENT_EIJI,")
            strSQL.AppendLine(" ISNULL(KANJI_NAME,'') AS PATIENT_KANJI,")
            strSQL.AppendLine(" ISNULL(KANA_NAME,'') AS PATIENT_KANA,")
            strSQL.AppendLine(" ISNULL(SEIBETSU,'') AS PATIENT_SEX,")
            strSQL.AppendLine(" ISNULL(BIRTH_DATE,'') AS PATIENT_BIRTH,")
            '---START Ver.1.7.0.0 CT引渡のため、身長、体重を追加 ADD By Watanabe 2021.10.15
            strSQL.AppendLine(" ISNULL(SHINCHO,'0') AS PATIENT_SHINCHO,")
            strSQL.AppendLine(" ISNULL(TAIJYU,'0') AS PATIENT_TAIJYU,")
            '---END Ver.1.7.0.0 CT引渡のため、身長、体重を追加 ADD By Watanabe 2021.10.15
            strSQL.AppendLine(" MODALITY_CD,")
            strSQL.AppendLine(" TERMINAL,")
            strSQL.AppendLine(" ISNULL(PORTABLE,'0') AS PORTABLE,")
            '---Ver.1.8.0.0 START 施設健診対応 ADD By Watanabe 2022.11.18
            If My.Settings.STUDY_DIV = "0" Then
                strSQL.AppendLine(" ISNULL(SITEHEALTH,'0') AS SITEHEALTH,")
            End If
            '---Ver.1.8.0.0 END 施設健診対応 ADD By Watanabe 2022.11.18
            strSQL.AppendLine(" KOUMKU_HIMBAN AS ORDER_MODALITY_CD ")
            strSQL.AppendLine("FROM ")

            Select Case My.Settings.STUDY_DIV
                Case "0"    '放射線
                    strSQL.AppendLine("vwMWM_XRAY ")
                Case "1"    '超音波
                    strSQL.AppendLine("vwMWM_US ")
                Case "2"    '内視鏡
                    strSQL.AppendLine("vwMWM_ES ")
            End Select
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" MWM_STATE = '1' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine(" DEL_FLG = '0' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine(" (CONVERT(varchar,JISSHIYOTEI_KAISHI_DATETIME,112) >= '" & DateAdd(DateInterval.Day, -1, Date.Today).ToString("yyyyMMdd") & "'")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine(" CONVERT(varchar,JISSHIYOTEI_KAISHI_DATETIME,112) <= '" & Date.Today.ToString("yyyyMMdd") & "') ")

            '///SQL文発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///構造体クラスの初期化
            Erase typOrder                  'オーダ情報
            Erase typPatient                '患者情報

            '///0件処理
            If dsData.Tables.Count = 0 Then
                'Call subDispMsg(0, "該当データ無し", strMsg)
                'Call subOutLog("該当データ無し", 0)
                Exit Sub
            End If

            If dsData.Tables(0).Rows.Count = 0 Then
                'Call subDispMsg(0, "該当データ無し", strMsg)
                'Call subOutLog("該当データ無し", 0)
                Exit Sub
            End If

            Call subDispMsg(0, dsData.Tables(0).Rows.Count & "件のデータを検知", strMsg)
            Call subOutLog(dsData.Tables(0).Rows.Count & "件のデータを検知", 0)

            Erase typOrder
            '///取得データ構造体クラスへ退避
            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1

                '///オーダ情報
                ReDim Preserve typOrder(lngCounter)
                typOrder(lngCounter) = New TYPE_TRAN_ORDER

                With typOrder(lngCounter)
                    .ORDER_ANO = dsData.Tables(0).Rows(lngCounter).Item("ORDER_ANO")
                    .MWM_ORDER_NO = dsData.Tables(0).Rows(lngCounter).Item("MWM_ORDER_NO")
                    .ORDER_DATE = dsData.Tables(0).Rows(lngCounter).Item("ORDER_DATE")
                    .ORDER_TIME = dsData.Tables(0).Rows(lngCounter).Item("ORDER_TIME")
                    .PATIENT_ANO = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_ANO")
                    '.PATIENT_ID = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_ID")
                    strPatientID = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_ID")
                    strPatientID = strPatientID.Trim.PadLeft(6).Replace(Space(1), "0")
                    .PATIENT_ID = strPatientID
                    .PATIENT_NAME_EIJI = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_EIJI")
                    .PATIENT_NAME_KANJI = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_KANJI")
                    .PATIENT_NAME_KANA = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_KANA")
                    .PATIENT_SEX = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_SEX")
                    .PATIENT_BIRTH = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_BIRTH")
                    '---START Ver.1.7.0.0 CT引渡のため、身長、体重を追加 ADD By Watanabe 2021.10.15
                    .PATIENT_SHINCHO = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_SHINCHO")
                    .PATIENT_TAIJYU = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_TAIJYU")
                    dcmlWork = .PATIENT_SHINCHO / 100
                    .PATIENT_SHINCHO = dcmlWork.ToString("0.0000")
                    dcmlWork = .PATIENT_TAIJYU
                    .PATIENT_TAIJYU = dcmlWork.ToString("0.00")
                    '---END Ver.1.7.0.0 CT引渡のため、身長、体重を追加 ADD By Watanabe 2021.10.15
                    .PATIENT_BIRTH = .PATIENT_BIRTH.Replace("/", "")
                    .TERMINAL = dsData.Tables(0).Rows(lngCounter).Item("TERMINAL")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")
                    .PORTABLE = dsData.Tables(0).Rows(lngCounter).Item("PORTABLE")
                    '.CHARCTERSET = dsData.Tables(0).Rows(lngCounter).Item("CHARCTERSET")
                    '.SCU_AE = dsData.Tables(0).Rows(lngCounter).Item("SCU_AE")
                    '.HOME_PATH = dsData.Tables(0).Rows(lngCounter).Item("HOME_PATH")
                    '---Ver.1.8.0.0 START 施設健診対応 ADD By Watanabe 2022.11.18
                    If My.Settings.STUDY_DIV = "0" Then
                        .SITEHELTH = dsData.Tables(0).Rows(lngCounter).Item("SITEHEALTH")
                    End If
                    '---Ver.1.8.0.0 END 施設健診対応 ADD By Watanabe 2022.11.18

                    dsModality = New DataSet
                    strSQL.Clear()
                    strSQL.AppendLine("SELECT ")
                    strSQL.AppendLine(" CHARCTERSET,")
                    strSQL.AppendLine(" SCU_AE,")
                    strSQL.AppendLine(" HOME_PATH ")
                    strSQL.AppendLine("FROM ")
                    strSQL.AppendLine(" MST_MODALITY ")
                    strSQL.AppendLine("WHERE ")
                    strSQL.AppendLine(" TERMINAL = '" & .TERMINAL & "' ")

                    dsModality = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

                    .CHARCTERSET = dsModality.Tables(0).Rows(0).Item("CHARCTERSET")
                    .SCU_AE = dsModality.Tables(0).Rows(0).Item("SCU_AE")
                    .HOME_PATH = dsModality.Tables(0).Rows(0).Item("HOME_PATH")

                    dsModality.Dispose()
                End With

            Next lngCounter

        Catch ex As Exception
            strMsg = ex.Message
        Finally
            dsData.Clear()
            dsData.Dispose()
            dsData = Nothing
            dsModality.Clear()
            dsModality.Dispose()
            dsModality = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub
#End Region

#Region "CT用 WL用Tempファイル作成処理(subMakeTempFileCT)"
    '*******************************************************************
    '* 機能概要　：CT用 WL用Tempファイル作成処理
    '* 　　　　　：本関数内でTempファイルのDICOM変換も行う
    '* 関数名称　：subMakeTempFileCT
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subMakeTempFileCT(ByVal typCT As TYPE_TRAN_ORDER)
        Dim strTempFile As New System.Text.StringBuilder
        Dim strTempFilePath As String = vbNullString
        Dim strMsg As String = vbNullString
        Dim strInData As New System.Text.StringBuilder
        Dim strOutData As New System.Text.StringBuilder
        Dim strInName As New System.Text.StringBuilder
        Dim strOutName As New System.Text.StringBuilder
        Dim strKana As String = vbNullString
        Dim strEiji As String = vbNullString
        Dim strPatientWK() As String
        Dim str1ST As String = vbNullString
        Dim str2ND As String = vbNullString

        If typCT Is Nothing Then Exit Sub

        Call subDispMsg(0, "CT用Tempファイル作成処理開始", strMsg)
        Call subOutLog("CT用Tempファイル作成処理開始", 0)

        With typCT

            If My.Settings.DEL_BEFOR_WL = True Then
                '///事前に過去作成WLファイルをBackUpフォルダに移動
                Call subDelMWM(typCT)
            End If

            strTempFile.Clear()
            strTempFile.AppendLine("(0040,0100) SQ []")                                             'Scheduled Procedure Step Sequence
            strTempFile.AppendLine("(fffe,e000) -")
            strTempFile.AppendLine("(0008,0060) CS [CT]")                                           'ModalityCD
            strTempFile.AppendLine("(0040,0001) AE [" & .SCU_AE & "]")                              'AE Title
            strTempFile.AppendLine("(0040,0002) DA [" & Now.ToString("yyyyMMdd") & "]")             '検査日
            strTempFile.AppendLine("(0040,0003) TM [" & Now.ToString("HHmmss") & "]")               '検査時刻
            strTempFile.AppendLine("(fffe,e00d) -")
            strTempFile.AppendLine("(fffe,e0dd) -")
            strTempFile.AppendLine("(0008,0005) CS [" & .CHARCTERSET & "]")              'Character Set
            strTempFile.AppendLine("(0008,0050) SH [" & .MWM_ORDER_NO & "]")                            'Accession Number

            '///デモ用2012.10.31
            Dim intProc As Integer = 0

            '///全角文字を変換(全角カナ氏名)
            strInName.Clear()
            strOutName.Clear()
            strKana = StrConv(.PATIENT_NAME_KANA, VbStrConv.Wide)

            If InStr(strKana, "　") > 0 Then
                strPatientWK = strKana.Split("　")
                str1ST = strPatientWK(0).Trim
                str2ND = strPatientWK(1).Trim
                '///苗字の変換
                strInName.Append(str1ST)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strOutName.ToString
                '///名前の変換
                strInName.Clear()
                strOutName.Clear()
                strInName.Append(str2ND)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strKana & "^" & strOutName.ToString
            Else
                strInName.Clear()
                strOutName.Clear()
                strInName.Append(strKana)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strOutName.ToString
            End If

            strEiji = .PATIENT_NAME_EIJI.Trim.Replace(" ", "^")

            If intProc = 0 Then
                intProc = InStr(.PATIENT_NAME_KANA.Trim, Space(1))
                If intProc > 0 Then

                    If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    Else
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    End If
                Else
                    If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace("　", "^") & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace("　", "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    Else
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    End If
                End If
            Else
                If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "]")
                ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                Else
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                End If
            End If

            '///デモ用の為 コメント 2012.10.31
            'strTempFile.AppendLine("(0010,0010) PN [" & .PATIENT_NAME_EIJI.Trim.Replace(Space(1), "^").ToUpper & "]")
            strTempFile.AppendLine("(0010,0020) LO [" & .PATIENT_ID & "]")                          '患者ID
            strTempFile.AppendLine("(0010,0030) DA [" & .PATIENT_BIRTH & "]") '患者生年月日

            '///取得データによる性別の編集
            Select Case .PATIENT_SEX                                          '患者性別
                Case "1", "M"
                    strTempFile.AppendLine("(0010,0040) CS [M]")
                Case "2", "F"
                    strTempFile.AppendLine("(0010,0040) CS [F]")
                Case "9", "O"
                    strTempFile.AppendLine("(0010,0040) CS [O]")
                Case Else
                    strTempFile.AppendLine("(0010,0040) CS []")
            End Select

            '---START Ver.1.7.0.0 CT引渡のため、身長、体重を追加 ADD By Watanabe 2021.10.15
            strTempFile.AppendLine("(0010,1020) DS [" & .PATIENT_SHINCHO & "]")
            strTempFile.AppendLine("(0010,1030) DS [" & .PATIENT_TAIJYU & "]")
            '---END Ver.1.7.0.0 CT引渡のため、身長、体重を追加 ADD By Watanabe 2021.10.15

            '///StudyInstanceUIDの取得
            .MWM_STUDY_INSTANCE_UID = fncGetStudyInstanceUID(strMsg)
            '---Ver.1.7.1.0 Study Instance UID重複防止の為Modality番号を付与 ADD By Watanabe 2022.04.15
            .MWM_STUDY_INSTANCE_UID &= "." & .MODALITY_NO

            strTempFile.AppendLine("(0020,000d) UI [" & .MWM_STUDY_INSTANCE_UID & "]")              'StudyInstanceUID

            '///Tempファイル生成
            '///ファイル名の重複を防ぐため。ファイル名にMillisecoundを追加 ADD By Watanabe 2014.02.18
            'strTempFilePath = typCT.HOME_PATH & .ORDER_NO & "_" & .PATIENT_ID & ".tmp"
            strTempFilePath = typCT.HOME_PATH & .MWM_ORDER_NO & "_" & .PATIENT_ID & "_" & Date.Now.Millisecond & ".tmp"
            Dim objsw As New System.IO.StreamWriter(strTempFilePath,
                                                   True,
                                                   System.Text.Encoding.GetEncoding(932))
            objsw.Write(strTempFile.ToString)
            objsw.Close()

            Call subDispMsg(0, strTempFilePath & "作成", strMsg)
            Call subOutLog(strTempFilePath & "作成", 0)

            '///DICOM変換
            Call subConvWL(strTempFilePath, strTempFilePath & ".wl")

            Call subDispMsg(0, "DICOM変換完了 " & strTempFilePath & ".wl", strMsg)
            Call subOutLog("DICOM変換完了 " & strTempFilePath & ".wl", 0)

        End With

    End Sub
#End Region

#Region "CR用 WL用Tempファイル作成処理(subMakeTempFileCR)"
    '*******************************************************************
    '* 機能概要　：CR用 WL用Tempファイル作成処理
    '* 　　　　　：本関数内でTempファイルのDICOM変換も行う
    '* 関数名称　：subMakeTempFile
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subMakeTempFileCR(ByVal typCR As TYPE_TRAN_ORDER)
        Dim strTempFile As New System.Text.StringBuilder
        Dim strTempFilePath As String = vbNullString
        Dim strMsg As String = vbNullString
        Dim strInData As New System.Text.StringBuilder
        Dim strOutData As New System.Text.StringBuilder
        Dim strInName As New System.Text.StringBuilder
        Dim strOutName As New System.Text.StringBuilder
        Dim strKana As String = vbNullString
        Dim strEiji As String = vbNullString
        Dim strEditKana As String()
        Dim strPatientWK() As String
        Dim str1ST As String = vbNullString
        Dim str2ND As String = vbNullString

        If typCR Is Nothing Then Exit Sub

        Call subDispMsg(0, "CR用Tempファイル作成処理開始", strMsg)
        Call subOutLog("CR用Tempファイル作成処理開始", 0)

        With typCR
            If My.Settings.DEL_BEFOR_WL = True Then
                '///事前に過去作成WLファイルをBackUpフォルダに移動
                'Call subDelMWM(typCR)
            End If

            '///SQL文生成
            strTempFile.Clear()
            strTempFile.AppendLine("(0040,0100) SQ []")                                             'Scheduled Procedure Step Sequence
            strTempFile.AppendLine("(fffe,e000) -")
            strTempFile.AppendLine("(0008,0060) CS [CR]")                                           'ModalityCD
            strTempFile.AppendLine("(0040,0001) AE [" & .SCU_AE & "]")                              'AE Title
            strTempFile.AppendLine("(0040,0002) DA [" & Now.ToString("yyyyMMdd") & "]")             '検査日
            strTempFile.AppendLine("(0040,0003) TM [" & Now.ToString("HHmmss") & "]")               '検査時刻
            '---Ver.1.8.0.0 START 施設健診対応 ADD By Watanabe 2022.11.18

            '---START Ver.1.9.0.0　施設健診を判断するための条件を追加 UpDated By Watanabe 2025.01.28 
            'If .SITEHELTH = "1" Then
            If .SITEHELTH = "1" And .PORTABLE = "1" Then
                '--―部位追加
                strTempFile.AppendLine("(0040,0008) SQ []")                                             'Scheduled Procedure Step Sequence
                strTempFile.AppendLine("(fffe,e000) -")
                '部位コード
                strTempFile.AppendLine("(0008,0100) SH [FCR020F-0001]")    ' Code Value
                strTempFile.AppendLine("(0008,0102) SH [DCM]")                                  ' Coding Scheme Designator
                strTempFile.AppendLine("(0008,0103) SH [1]")                                    ' Coding Scheme Version
                '部位名
                'strTempFile.AppendLine("(0008,0104) LO [施設健診胸部]")    ' Code Meaning
                strTempFile.AppendLine("(0008,0104) LO [胸部ポータブル(A-P)2.4]")    ' Code Meaning
                'strTempFile.AppendLine("(0008,0104) LO []")    ' Code Meaning

                strTempFile.AppendLine("(fffe,e00d) -")
                strTempFile.AppendLine("(fffe,e0dd) -")
                strTempFile.AppendLine("(0040,0009) SH [1]")
            End If
            '---Ver.1.8.0.0 END 施設健診対応 ADD By Watanabe 2022.11.18

            strTempFile.AppendLine("(fffe,e00d) -")
            strTempFile.AppendLine("(fffe,e0dd) -")
            If .SITEHELTH = "1" Then
                strTempFile.AppendLine("(0008,0005) CS [\ISO 2022 IR 13\ISO 2022 IR 87]")                         'Character Set
            Else
                strTempFile.AppendLine("(0008,0005) CS [" & .CHARCTERSET & "]")                         'Character Set
            End If
            strTempFile.AppendLine("(0008,0050) SH [" & .MWM_ORDER_NO & "]")                            'Accession Number

            Dim intProc As Integer = 0
            '///全角文字を変換(全角カナ氏名)
            strInName.Clear()
            strOutName.Clear()
            strKana = StrConv(.PATIENT_NAME_KANA, VbStrConv.Wide)

            If InStr(strKana, "　") > 0 Then
                strPatientWK = strKana.Split("　")
                str1ST = strPatientWK(0).Trim
                str2ND = strPatientWK(1).Trim
                '///苗字の変換
                strInName.Append(str1ST)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strOutName.ToString
                '///名前の変換
                strInName.Clear()
                strOutName.Clear()
                strInName.Append(str2ND)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strKana & "^" & strOutName.ToString
            Else
                strInName.Clear()
                strOutName.Clear()
                strInName.Append(strKana)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strOutName.ToString
            End If


            strEiji = .PATIENT_NAME_EIJI.Trim.Replace(" ", "^")

            intProc = InStr(.PATIENT_NAME_KANA.Trim, "　")
            If intProc = 0 Then
                intProc = InStr(.PATIENT_NAME_KANA.Trim, Space(1))
                If intProc > 0 Then

                    If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                        'strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "=]")
                        strTempFile.AppendLine("(0010,0010) PN [" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "==]")
                    ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    Else
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    End If
                Else
                    If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace("　", "^") & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace("　", "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                        'strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "=]")
                        strTempFile.AppendLine("(0010,0010) PN [" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "==]")
                    ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    Else
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    End If
                End If
            Else
                If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "]")
                ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                    'strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "=]")
                    strTempFile.AppendLine("(0010,0010) PN [" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "==]")
                ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                Else
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                End If
            End If
            strTempFile.AppendLine("(0010,0020) LO [" & .PATIENT_ID & "]")                          '患者ID
            strTempFile.AppendLine("(0010,0030) DA [" & .PATIENT_BIRTH & "]")                       '患者生年月日

            '///取得データによる性別の編集
            Select Case .PATIENT_SEX                                                                '患者性別
                Case "1", "M"
                    strTempFile.AppendLine("(0010,0040) CS [M]")
                Case "2", "F"
                    strTempFile.AppendLine("(0010,0040) CS [F]")
                Case "9", "O"
                    strTempFile.AppendLine("(0010,0040) CS [O]")
                Case Else
                    strTempFile.AppendLine("(0010,0040) CS []")
            End Select

            '///StudyInstanceUIDの取得
            .MWM_STUDY_INSTANCE_UID = fncGetStudyInstanceUID(strMsg)
            '---Ver.1.7.1.0 Study Instance UID重複防止の為Modality番号を付与 ADD By Watanabe 2022.04.15
            .MWM_STUDY_INSTANCE_UID &= "." & .MODALITY_NO

            strTempFile.AppendLine("(0020,000d) UI [" & .MWM_STUDY_INSTANCE_UID & "]")              'StudyInstanceUID
            strTempFile.AppendLine("(0032,1060) LO []")              'Request Procedure Description

            '///Tempファイル生成
            strTempFilePath = typCR.HOME_PATH & .MWM_ORDER_NO & "_" & .PATIENT_ID & ".tmp"
            Dim objsw As New System.IO.StreamWriter(strTempFilePath,
                                                   True,
                                                   System.Text.Encoding.GetEncoding(932))
            objsw.Write(strTempFile.ToString)
            objsw.Close()

            Call subDispMsg(0, strTempFilePath & "作成", strMsg)
            Call subOutLog(strTempFilePath & "作成", 0)

            '///DICOM変換
            Call subConvWL(strTempFilePath, strTempFilePath & ".wl")

            Call subDispMsg(0, "DICOM変換完了 " & strTempFilePath & ".wl", strMsg)
            Call subOutLog("DICOM変換完了 " & strTempFilePath & ".wl", 0)

        End With

    End Sub
#End Region

#Region "RF用 WL用Tempファイル作成処理(subMakeTempFileRF)"
    '*******************************************************************
    '* 機能概要　：RF用 WL用Tempファイル作成処理
    '* 　　　　　：本関数内でTempファイルのDICOM変換も行う
    '* 関数名称　：subMakeTempFileRF
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subMakeTempFileRF(ByVal typRF As TYPE_TRAN_ORDER)
        Dim strTempFile As New System.Text.StringBuilder
        Dim strTempFilePath As String = vbNullString
        Dim strMsg As String = vbNullString
        Dim strInData As New System.Text.StringBuilder
        Dim strOutData As New System.Text.StringBuilder
        Dim strInName As New System.Text.StringBuilder
        Dim strOutName As New System.Text.StringBuilder
        Dim strKana As String = vbNullString
        Dim strEiji As String = vbNullString
        Dim strPatientWK() As String
        Dim str1ST As String = vbNullString
        Dim str2ND As String = vbNullString

        If typRF Is Nothing Then Exit Sub

        Call subDispMsg(0, "RF用Tempファイル作成処理開始", strMsg)
        Call subOutLog("RF用Tempファイル作成処理開始", 0)

        With typRF
            If My.Settings.DEL_BEFOR_WL = True Then
                '///事前に過去作成WLファイルをBackUpフォルダに移動
                Call subDelMWM(typRF)
            End If

            strTempFile.Clear()
            strTempFile.AppendLine("(0040,0100) SQ []")                                             'Scheduled Procedure Step Sequence
            strTempFile.AppendLine("(fffe,e000) -")
            strTempFile.AppendLine("(0008,0060) CS [RF]")                                           'ModalityCD
            strTempFile.AppendLine("(0040,0001) AE [" & .SCU_AE & "]")                              'AE Title
            strTempFile.AppendLine("(0040,0002) DA [" & Now.ToString("yyyyMMdd") & "]")             '検査日
            strTempFile.AppendLine("(0040,0003) TM [" & Now.ToString("HHmmss") & "]")               '検査時刻
            strTempFile.AppendLine("(fffe,e00d) -")
            strTempFile.AppendLine("(fffe,e0dd) -")
            strTempFile.AppendLine("(0008,0005) CS [" & .CHARCTERSET & "]")              'Character Set
            strTempFile.AppendLine("(0008,0050) SH [" & .MWM_ORDER_NO & "]")                            'Accession Number

            '///全角文字を変換(全角カナ氏名)
            strInName.Clear()
            strOutName.Clear()
            strKana = StrConv(.PATIENT_NAME_KANA, VbStrConv.Wide)

            If InStr(strKana, "　") > 0 Then
                strPatientWK = strKana.Split("　")
                str1ST = strPatientWK(0).Trim
                str2ND = strPatientWK(1).Trim
                '///苗字の変換
                strInName.Append(str1ST)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strOutName.ToString
                '///名前の変換
                strInName.Clear()
                strOutName.Clear()
                strInName.Append(str2ND)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strKana & "^" & strOutName.ToString
            Else
                strInName.Clear()
                strOutName.Clear()
                strInName.Append(strKana)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strOutName.ToString
            End If


            strEiji = .PATIENT_NAME_EIJI.Trim.Replace(" ", "^")

            Dim intProc As Integer

            intProc = InStr(.PATIENT_NAME_KANA.Trim, "　")
            If intProc = 0 Then
                intProc = InStr(.PATIENT_NAME_KANA.Trim, Space(1))
                If intProc > 0 Then

                    If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    Else
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    End If
                Else
                    If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace("　", "^") & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace("　", "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    Else
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    End If
                End If
            Else
                If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "]")
                ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                Else
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                End If
            End If

            strTempFile.AppendLine("(0010,0020) LO [" & .PATIENT_ID & "]")                          '患者ID
            strTempFile.AppendLine("(0010,0030) DA [" & .PATIENT_BIRTH & "]") '患者生年月日

            '///取得データによる性別の編集
            Select Case .PATIENT_SEX                                          '患者性別
                Case "1", "M"
                    strTempFile.AppendLine("(0010,0040) CS [M]")
                Case "2", "F"
                    strTempFile.AppendLine("(0010,0040) CS [F]")
                Case "9", "O"
                    strTempFile.AppendLine("(0010,0040) CS [O]")
                Case Else
                    strTempFile.AppendLine("(0010,0040) CS []")
            End Select

            '///StudyInstanceUIDの取得
            .MWM_STUDY_INSTANCE_UID = fncGetStudyInstanceUID(strMsg)
            '---Ver.1.7.1.0 Study Instance UID重複防止の為Modality番号を付与 ADD By Watanabe 2022.04.15
            .MWM_STUDY_INSTANCE_UID &= "." & .MODALITY_NO

            strTempFile.AppendLine("(0020,000d) UI [" & .MWM_STUDY_INSTANCE_UID & "]")              'StudyInstanceUID

            '///Tempファイル生成
            strTempFilePath = .HOME_PATH & .MWM_ORDER_NO & "_" & .PATIENT_ID & ".tmp"
            Dim objsw As New System.IO.StreamWriter(strTempFilePath,
                                                   True,
                                                   System.Text.Encoding.GetEncoding(932))
            objsw.Write(strTempFile.ToString)
            objsw.Close()

            Call subDispMsg(0, strTempFilePath & "作成", strMsg)
            Call subOutLog(strTempFilePath & "作成", 0)

            '///DICOM変換
            Call subConvWL(strTempFilePath, strTempFilePath & ".wl")

            Call subDispMsg(0, "DICOM変換完了 " & strTempFilePath & ".wl", strMsg)
            Call subOutLog("DICOM変換完了 " & strTempFilePath & ".wl", 0)

        End With

    End Sub
#End Region

#Region "US用 WL用Tempファイル作成処理(subMakeTempFileUS)"
    '*******************************************************************
    '* 機能概要　：US用 WL用Tempファイル作成処理
    '* 　　　　　：本関数内でTempファイルのDICOM変換も行う
    '* 関数名称　：subMakeTempFile
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subMakeTempFileUS(ByVal typUS As TYPE_TRAN_ORDER)
        Dim strTempFile As New System.Text.StringBuilder
        Dim strTempFilePath As String = vbNullString
        Dim strMsg As String = vbNullString
        Dim strInData As New System.Text.StringBuilder
        Dim strOutData As New System.Text.StringBuilder
        Dim strInName As New System.Text.StringBuilder
        Dim strOutName As New System.Text.StringBuilder
        Dim strKana As String = vbNullString
        Dim intCount As Integer = 1
        Dim intCounter As Integer = 0

        If typUS Is Nothing Then Exit Sub

        Call subDispMsg(0, "US用Tempファイル作成処理開始", strMsg)
        Call subOutLog("US用Tempファイル作成処理開始", 0)

        With typUS
            ''---Portable2台対応 2021.09.24 ADD By Watanabe
            'If .TERMINAL <> "PRIM_006" And .TERMINAL <> "PRIM_007" Then
            '    intCount = 0
            'End If

            'For intCounter = 0 To intCount

            'If My.Settings.DEL_BEFOR_WL = True And .PORTABLE = "0" Then
            ''If .TERMINAL <> "PRIM_007" Then
            'If .TERMINAL = "PRIM_006" Then
            '    If InStr(.HOME_PATH, "US4") > 0 Then
            '        .HOME_PATH = .HOME_PATH.Replace("US4", "US2")
            '        .SCU_AE = "XARIO200_1"
            '        .CHARCTERSET = "ISO IR 100"
            '    Else
            '        .HOME_PATH = .HOME_PATH.Replace("US2", "US4")
            '        .SCU_AE = "VIVID_S60"
            '        .CHARCTERSET = "ISO 2022 IR 6"
            '    End If
            'Else
            '///事前に過去作成WLファイルをBackUpフォルダに移動
            ''Call subDelMWM(typUS)
            'End If
            ''Else    '---Portable2台対応 2021.09.24 ADD By Watanabe
            ''If InStr(.HOME_PATH, "US5") > 0 Then
            ''    .HOME_PATH = .HOME_PATH.Replace("US5", "US6")
            ''    .SCU_AE = "VSV72225"
            ''Else
            ''    .HOME_PATH = .HOME_PATH.Replace("US6", "US5")
            ''        .SCU_AE = "VSV72214"
            ''    End If
            ''End If
            'If .TERMINAL <> "PRIM_007" Then
            '    Call subDelMWM(typUS)
            'End If
            If My.Settings.DEL_BEFOR_WL = True Then
                Call subDelMWM(typUS)
            End If

            '///Tempファイル生成
            strTempFile.Clear()
            strTempFile.AppendLine("(0040,0100) SQ []")                                                                     'Scheduled Procedure Step Sequence
            strTempFile.AppendLine("(fffe,e000) -")
            strTempFile.AppendLine("(0008,0060) CS [US]")                                                                   'ModalityCD
            strTempFile.AppendLine("(0040,0001) AE [" & .SCU_AE & "]")                                                      'AE Title
            strTempFile.AppendLine("(0040,0002) DA [" & Now.ToString("yyyyMMdd") & "]")                                     '検査日
            strTempFile.AppendLine("(0040,0003) TM [" & Now.ToString("HHmmss") & "]")                                       '検査時刻
            strTempFile.AppendLine("(fffe,e00d) -")
            strTempFile.AppendLine("(fffe,e0dd) -")
            strTempFile.AppendLine("(0008,0005) CS [" & .CHARCTERSET & "]")                                                 'Character Set
            strTempFile.AppendLine("(0008,0050) SH [" & .MWM_ORDER_NO & "]")                                                'Accession Number

            strTempFile.AppendLine("(0010,0010) PN [" & .PATIENT_NAME_EIJI.Trim.Replace(Space(1), "^").ToUpper & "]")
            strTempFile.AppendLine("(0010,0020) LO [" & .PATIENT_ID & "]")                                                  '患者ID
            strTempFile.AppendLine("(0010,0030) DA [" & .PATIENT_BIRTH & "]")                                               '患者生年月日

            '///取得データによる性別の編集
            Select Case .PATIENT_SEX                                                                                        '患者性別
                Case "1", "M"
                    strTempFile.AppendLine("(0010,0040) CS [M]")
                Case "2", "F"
                    strTempFile.AppendLine("(0010,0040) CS [F]")
                Case "9", "O"
                    strTempFile.AppendLine("(0010,0040) CS [O]")
                Case Else
                    strTempFile.AppendLine("(0010,0040) CS []")
            End Select

            '///StudyInstanceUIDの取得
            .MWM_STUDY_INSTANCE_UID = fncGetStudyInstanceUID(strMsg)
            '---Ver.1.7.1.0 Study Instance UID重複防止の為Modality番号を付与 ADD By Watanabe 2022.04.15
            .MWM_STUDY_INSTANCE_UID &= "." & .MODALITY_NO

            strTempFile.AppendLine("(0020,000d) UI [" & .MWM_STUDY_INSTANCE_UID & "]")                                      'StudyInstanceUID


            strTempFilePath = .HOME_PATH & .MWM_ORDER_NO & "_" & .PATIENT_ID & ".tmp"

            Dim objsw As New System.IO.StreamWriter(strTempFilePath,
                                                       True,
                                                       System.Text.Encoding.GetEncoding(932))
            objsw.Write(strTempFile.ToString)
            objsw.Close()

            Call subDispMsg(0, strTempFilePath & "作成", strMsg)
            Call subOutLog(strTempFilePath & "作成", 0)

            '///DICOM変換
            Call subConvWL(strTempFilePath, strTempFilePath & ".wl")

            Call subDispMsg(0, "DICOM変換完了 " & strTempFilePath & ".wl", strMsg)
            Call subOutLog("DICOM変換完了 " & strTempFilePath & ".wl", 0)
            'Next intCounter

        End With

    End Sub
#End Region

#Region "MR用 WL用Tempファイル作成処理(subMakeTempFileMR)"
    '*******************************************************************
    '* 機能概要　：MR用 WL用Tempファイル作成処理
    '* 　　　　　：本関数内でTempファイルのDICOM変換も行う
    '* 関数名称　：subMakeTempFileMR
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subMakeTempFileMR(ByVal typMR As TYPE_TRAN_ORDER)
        Dim strTempFile As New System.Text.StringBuilder
        Dim strTempFilePath As String = vbNullString
        Dim strMsg As String = vbNullString
        Dim strInData As New System.Text.StringBuilder
        Dim strOutData As New System.Text.StringBuilder
        Dim strInName As New System.Text.StringBuilder
        Dim strOutName As New System.Text.StringBuilder
        Dim strKana As String = vbNullString
        Dim strEiji As String = vbNullString
        Dim strPatientWK() As String
        Dim str1ST As String = vbNullString
        Dim str2ND As String = vbNullString

        If typMR Is Nothing Then Exit Sub

        Call subDispMsg(0, "MR用Tempファイル作成処理開始", strMsg)
        Call subOutLog("MR用Tempファイル作成処理開始", 0)

        With typMR
            If My.Settings.DEL_BEFOR_WL = True Then
                '///事前に過去作成WLファイルをBackUpフォルダに移動
                Call subDelMWM(typMR)
            End If

            strTempFile.Clear()
            strTempFile.AppendLine("(0040,0100) SQ []")                                             'Scheduled Procedure Step Sequence
            strTempFile.AppendLine("(fffe,e000) -")
            strTempFile.AppendLine("(0008,0060) CS [MR]")                                           'ModalityCD
            strTempFile.AppendLine("(0040,0001) AE [" & .SCU_AE & "]")                              'AE Title
            strTempFile.AppendLine("(0040,0002) DA [" & Now.ToString("yyyyMMdd") & "]")             '検査日
            strTempFile.AppendLine("(0040,0003) TM [" & Now.ToString("HHmmss") & "]")               '検査時刻
            strTempFile.AppendLine("(0040,0009) SH [5]")                                            '検査時刻
            strTempFile.AppendLine("(fffe,e00d) -")
            strTempFile.AppendLine("(fffe,e0dd) -")
            strTempFile.AppendLine("(0040,1001) SH [6]")                                            '検査時刻
            strTempFile.AppendLine("(0008,0005) CS [" & .CHARCTERSET & "]")              'Character Set
            strTempFile.AppendLine("(0008,0050) SH [" & .MWM_ORDER_NO & "]")                            'Accession Number

            '///全角文字を変換(全角カナ氏名)
            strInName.Clear()
            strOutName.Clear()
            strKana = StrConv(.PATIENT_NAME_KANA, VbStrConv.Wide)

            If InStr(strKana, "　") > 0 Then
                strPatientWK = strKana.Split("　")
                str1ST = strPatientWK(0).Trim
                str2ND = strPatientWK(1).Trim
                '///苗字の変換
                strInName.Append(str1ST)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strOutName.ToString
                '///名前の変換
                strInName.Clear()
                strOutName.Clear()
                strInName.Append(str2ND)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strKana & "^" & strOutName.ToString
            Else
                strInName.Clear()
                strOutName.Clear()
                strInName.Append(strKana)
                nkf32.NkfConvert(strOutName, strInName)
                strKana = strOutName.ToString
            End If

            strEiji = .PATIENT_NAME_EIJI.Trim.Replace(" ", "^")

            Dim intProc As Integer

            intProc = InStr(.PATIENT_NAME_KANA.Trim, "　")
            If intProc = 0 Then
                intProc = InStr(.PATIENT_NAME_KANA.Trim, Space(1))
                If intProc > 0 Then

                    If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    Else
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    End If
                Else
                    If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace("　", "^") & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace("　", "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                    ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    Else
                        strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                    End If
                End If
            Else
                If Trim(.CHARCTERSET) = "\ISO 2022 IR 87" Then                      'ローマ字＋全角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "]")
                ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 87\ISO 2022 IR 13" Then   'ローマ字＋全角カナ＋半角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & strKana.Trim.Replace(Space(1), "^") & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                ElseIf Trim(.CHARCTERSET) = "\ISO 2022 IR 13" Then                  'ローマ字＋半角カナ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "=" & StrConv(.PATIENT_NAME_KANA, VbStrConv.Narrow) & "]")
                ElseIf .CHARCTERSET = "" Then                                       'ローマ字のみ
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                Else
                    strTempFile.AppendLine("(0010,0010) PN [" & strEiji.Trim & "]")
                End If
            End If

            strTempFile.AppendLine("(0010,0020) LO [" & .PATIENT_ID & "]")                          '患者ID
            strTempFile.AppendLine("(0010,0030) DA [" & .PATIENT_BIRTH & "]") '患者生年月日

            '///取得データによる性別の編集
            Select Case .PATIENT_SEX                                          '患者性別
                Case "1", "M"
                    strTempFile.AppendLine("(0010,0040) CS [M]")
                Case "2", "F"
                    strTempFile.AppendLine("(0010,0040) CS [F]")
                Case "9", "O"
                    strTempFile.AppendLine("(0010,0040) CS [O]")
                Case Else
                    strTempFile.AppendLine("(0010,0040) CS []")
            End Select

            '///StudyInstanceUIDの取得
            .MWM_STUDY_INSTANCE_UID = fncGetStudyInstanceUID(strMsg)
            '---Ver.1.7.1.0 Study Instance UID重複防止の為Modality番号を付与 ADD By Watanabe 2022.04.15
            .MWM_STUDY_INSTANCE_UID &= "." & .MODALITY_NO

            strTempFile.AppendLine("(0020,000d) UI [" & .MWM_STUDY_INSTANCE_UID & "]")              'StudyInstanceUID
            strTempFile.AppendLine("(0010,21C0) US [0001]")              'StudyInstanceUID

            '///Tempファイル生成
            strTempFilePath = typMR.HOME_PATH & .MWM_ORDER_NO & "_" & .PATIENT_ID & ".tmp"
            Dim objsw As New System.IO.StreamWriter(strTempFilePath,
                                                   True,
                                                   System.Text.Encoding.GetEncoding(932))
            objsw.Write(strTempFile.ToString)
            objsw.Close()

            Call subDispMsg(0, strTempFilePath & "作成", strMsg)
            Call subOutLog(strTempFilePath & "作成", 0)

            '///DICOM変換
            Call subConvWL(strTempFilePath, strTempFilePath & ".wl")

            Call subDispMsg(0, "DICOM変換完了 " & strTempFilePath & ".wl", strMsg)
            Call subOutLog("DICOM変換完了 " & strTempFilePath & ".wl", 0)

        End With

    End Sub
#End Region

#Region "XA用 WL用Tempファイル作成処理(subMakeTempFileXA)"
    '*******************************************************************
    '* 機能概要　：XA用 WL用Tempファイル作成処理
    '* 　　　　　：本関数内でTempファイルのDICOM変換も行う
    '* 関数名称　：subMakeTempFileXA
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subMakeTempFileXA(ByVal typXA As TYPE_TRAN_ORDER)
        Dim strTempFile As New System.Text.StringBuilder
        Dim strTempFilePath As String = vbNullString
        Dim strMsg As String = vbNullString
        Dim strInData As New System.Text.StringBuilder
        Dim strOutData As New System.Text.StringBuilder
        Dim strInName As New System.Text.StringBuilder
        Dim strOutName As New System.Text.StringBuilder
        Dim strKana As String = vbNullString

        If typXA Is Nothing Then Exit Sub

        Call subDispMsg(0, "XA用Tempファイル作成処理開始", strMsg)
        Call subOutLog("XA用Tempファイル作成処理開始", 0)

        With typXA
            If My.Settings.DEL_BEFOR_WL = True Then
                '///事前に過去作成WLファイルをBackUpフォルダに移動
                Call subDelMWM(typXA)
            End If

            strTempFile.Clear()
            strTempFile.AppendLine("(0040,0100) SQ []")                                             'Scheduled Procedure Step Sequence
            strTempFile.AppendLine("(fffe,e000) -")
            strTempFile.AppendLine("(0008,0060) CS [XA]")                                           'ModalityCD
            strTempFile.AppendLine("(0040,0001) AE [" & .SCU_AE & "]")                              'AE Title
            strTempFile.AppendLine("(0040,0002) DA [" & Now.ToString("yyyyMMdd") & "]")             '検査日
            strTempFile.AppendLine("(0040,0003) TM [" & Now.ToString("HHmmss") & "]")               '検査時刻
            strTempFile.AppendLine("(fffe,e00d) -")
            strTempFile.AppendLine("(fffe,e0dd) -")
            strTempFile.AppendLine("(0008,0005) CS [" & .CHARCTERSET & "]")              'Character Set
            strTempFile.AppendLine("(0008,0050) SH [" & .MWM_ORDER_NO & "]")                            'Accession Number

            strTempFile.AppendLine("(0010,0010) PN [" & .PATIENT_NAME_EIJI.Trim.Replace(Space(1), "^").ToUpper & "]")
            strTempFile.AppendLine("(0010,0020) LO [" & .PATIENT_ID & "]")                          '患者ID
            strTempFile.AppendLine("(0010,0030) DA [" & .PATIENT_BIRTH & "]") '患者生年月日

            '///取得データによる性別の編集
            Select Case .PATIENT_SEX                                          '患者性別
                Case "1", "M"
                    strTempFile.AppendLine("(0010,0040) CS [M]")
                Case "2", "F"
                    strTempFile.AppendLine("(0010,0040) CS [F]")
                Case "9", "O"
                    strTempFile.AppendLine("(0010,0040) CS [O]")
                Case Else
                    strTempFile.AppendLine("(0010,0040) CS []")
            End Select

            '///StudyInstanceUIDの取得
            .MWM_STUDY_INSTANCE_UID = fncGetStudyInstanceUID(strMsg)
            '---Ver.1.7.1.0 Study Instance UID重複防止の為Modality番号を付与 ADD By Watanabe 2022.04.15
            .MWM_STUDY_INSTANCE_UID &= "." & .MODALITY_NO

            strTempFile.AppendLine("(0020,000d) UI [" & .MWM_STUDY_INSTANCE_UID & "]")              'StudyInstanceUID

            '///Tempファイル生成
            strTempFilePath = typXA.HOME_PATH & .MWM_ORDER_NO & "_" & .PATIENT_ID & ".tmp"
            Dim objsw As New System.IO.StreamWriter(strTempFilePath,
                                                   True,
                                                   System.Text.Encoding.GetEncoding(932))
            objsw.Write(strTempFile.ToString)
            objsw.Close()

            Call subDispMsg(0, strTempFilePath & "作成", strMsg)
            Call subOutLog(strTempFilePath & "作成", 0)

            '///DICOM変換
            Call subConvWL(strTempFilePath, strTempFilePath & ".wl")

            Call subDispMsg(0, "DICOM変換完了 " & strTempFilePath & ".wl", strMsg)
            Call subOutLog("DICOM変換完了 " & strTempFilePath & ".wl", 0)

        End With

    End Sub
#End Region

#Region "ES用 WL用Tempファイル作成処理(subMakeTempFileES)"
    '*******************************************************************
    '* 機能概要　：ES用 WL用Tempファイル作成処理
    '* 　　　　　：本関数内でTempファイルのDICOM変換も行う
    '* 関数名称　：subMakeTempFile
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subMakeTempFileES(ByVal typES As TYPE_TRAN_ORDER)
        Dim strTempFile As New System.Text.StringBuilder
        Dim strTempFilePath As String = vbNullString
        Dim strMsg As String = vbNullString
        Dim strInData As New System.Text.StringBuilder
        Dim strOutData As New System.Text.StringBuilder
        Dim strInName As New System.Text.StringBuilder
        Dim strOutName As New System.Text.StringBuilder
        Dim strKana As String = vbNullString

        If typES Is Nothing Then Exit Sub

        Call subDispMsg(0, "ES用Tempファイル作成処理開始", strMsg)
        Call subOutLog("ES用Tempファイル作成処理開始", 0)

        With typES
            If My.Settings.DEL_BEFOR_WL = True Then
                '///事前に過去作成WLファイルをBackUpフォルダに移動
                Call subDelMWM(typES)
            End If

            strTempFile.Clear()
            strTempFile.AppendLine("(0040,0100) SQ []")                                             'Scheduled Procedure Step Sequence
            strTempFile.AppendLine("(fffe,e000) -")
            strTempFile.AppendLine("(0008,0060) CS [ES]")                                           'ModalityCD
            strTempFile.AppendLine("(0040,0001) AE [" & .SCU_AE & "]")                              'AE Title
            strTempFile.AppendLine("(0040,0002) DA [" & Now.ToString("yyyyMMdd") & "]")             '検査日
            strTempFile.AppendLine("(0040,0003) TM [" & Now.ToString("HHmmss") & "]")               '検査時刻
            strTempFile.AppendLine("(fffe,e00d) -")
            strTempFile.AppendLine("(fffe,e0dd) -")
            strTempFile.AppendLine("(0008,0005) CS [" & .CHARCTERSET & "]")              'Character Set
            strTempFile.AppendLine("(0008,0050) SH [" & .MWM_ORDER_NO & "]")                            'Accession Number

            strTempFile.AppendLine("(0010,0010) PN [" & .PATIENT_NAME_KANA.Trim.Replace(Space(1), "^").ToUpper & "]")
            strTempFile.AppendLine("(0010,0020) LO [" & .PATIENT_ID & "]")                          '患者ID
            strTempFile.AppendLine("(0010,0030) DA [" & .PATIENT_BIRTH & "]") '患者生年月日

            '///取得データによる性別の編集
            Select Case .PATIENT_SEX                                          '患者性別
                Case "1", "M"
                    strTempFile.AppendLine("(0010,0040) CS [M]")
                Case "2", "F"
                    strTempFile.AppendLine("(0010,0040) CS [F]")
                Case "9", "O"
                    strTempFile.AppendLine("(0010,0040) CS [O]")
                Case Else
                    strTempFile.AppendLine("(0010,0040) CS []")
            End Select

            '///StudyInstanceUIDの取得
            .MWM_STUDY_INSTANCE_UID = fncGetStudyInstanceUID(strMsg)
            '---Ver.1.7.1.0 Study Instance UID重複防止の為Modality番号を付与 ADD By Watanabe 2022.04.15
            .MWM_STUDY_INSTANCE_UID &= "." & .MODALITY_NO

            strTempFile.AppendLine("(0020,000d) UI [" & .MWM_STUDY_INSTANCE_UID & "]")              'StudyInstanceUID

            '///Tempファイル生成
            strTempFilePath = typES.HOME_PATH & .MWM_ORDER_NO & "_" & .PATIENT_ID & ".tmp"
            Dim objsw As New System.IO.StreamWriter(strTempFilePath,
                                                   True,
                                                   System.Text.Encoding.GetEncoding(932))
            objsw.Write(strTempFile.ToString)
            objsw.Close()

            Call subDispMsg(0, strTempFilePath & "作成", strMsg)
            Call subOutLog(strTempFilePath & "作成", 0)

            '///DICOM変換
            Call subConvWL(strTempFilePath, strTempFilePath & ".wl")

            Call subDispMsg(0, "DICOM変換完了 " & strTempFilePath & ".wl", strMsg)
            Call subOutLog("DICOM変換完了 " & strTempFilePath & ".wl", 0)

        End With

    End Sub
#End Region

#Region "WL用TempファイルDICOM変換(subConvWL)"
    '*******************************************************************
    '* 機能概要　：WL用TempファイルDICOM変換
    '* 関数名称　：subConvWL
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subConvWL(ByVal TempFileName As String, ByVal WorkListFileName As String)
        Dim strBuf As String
        Dim intRet As Integer

        Try
            Call subOutLog("DumpMWMFile Start", 0)

            '********************
            '* 起動文字列の編集 *
            '********************
            strBuf = """" & My.Settings.DUMP2DCM_PATH & Space(1) & """-v -g """ & TempFileName & """ """ & WorkListFileName & """"

            Call subOutLog("DumpMWMFile 起動パラメータ = " & strBuf, 0)

            '**********************
            '* 起動(完了まで待つ) *
            '**********************
            intRet = Shell(strBuf, AppWinStyle.MinimizedNoFocus, True, 6000)

            Call subOutLog("DumpMWMFile END", 0)

        Catch ex As Exception

            Call subOutLog("DumpMWMFile " & ex.Message, 1)

        End Try
    End Sub
#End Region

#Region "SCU AE Title取得(subGetSCU)"
    '*******************************************************************
    '* 機能概要　：SCU AE Title取得
    '* 関数名称　：subGetSCU
    '* 引　数　　：strMsg　・・・エラーメッセージ退避用
    '* 戻り値　　：String　・・・AE Title
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subGetSCU(ByRef strMsg)
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("ISNULL(MODALITY_CD,'') AS MODALITY_CD,")
            strSQL.AppendLine("ISNULL(SCP_AE,'') AS SCP_AE,")
            strSQL.AppendLine("ISNULL(SCU_AE,'') AS SCU_AE,")
            strSQL.AppendLine("ISNULL(SCU_IP,'') AS SCU_IP,")
            strSQL.AppendLine("SCU_PORT,")
            strSQL.AppendLine("ISNULL(CHARCTERSET,'') AS CHARCTERSET ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_MODALITY ")
            'strSQL.AppendLine("WHERE ")
            'strSQL.AppendLine("TERMINAL = '" & My.Settings.TERMINAL & "' ")
            'strSQL.AppendLine("AND ")
            'strSQL.AppendLine("MODALITY_CD = 'CT' ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///構造体クラスの初期化
            typModality = Nothing
            typModality = New clsMstModality

            '///取得データを構造体クラスへ退避
            With typModality
                .MODALITY_NO = dsData.Tables(0).Rows(0).Item("MODALITY_NO")
                .MODALITY_CD = dsData.Tables(0).Rows(0).Item("MODALITY_CD")
                .SCP_AE = dsData.Tables(0).Rows(0).Item("SCP_AE")
                .SCU_AE = dsData.Tables(0).Rows(0).Item("SCU_AE")
                .SCU_IP = dsData.Tables(0).Rows(0).Item("SCU_IP")
                .SCU_PORT = dsData.Tables(0).Rows(0).Item("SCU_PORT")
                .CHARCTERSET = dsData.Tables(0).Rows(0).Item("CHARCTERSET")
            End With
        Catch ex As Exception
            strMsg = ex.Message
        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try

    End Sub
#End Region

#Region "StudyInstanceUID取得(fncGetStudyInstanceUID)"
    '*************************************************************************
    '* 機能概要　：StudyInstanceUID取得
    '* 関数名称　：fncGetStudyInstanceUID
    '* 引　数　　：strMsg　・・・エラーメッセージ退避用
    '* 戻り値　　：String　・・・StudyInstanceUID
    '* 作成日　　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '*************************************************************************
    Private Function fncGetStudyInstanceUID(ByRef strMsg As String) As String
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim strWork As String
        Dim strWorkDate() As String
        Dim intRet As Integer
        Dim dsWork As New DataSet
        Dim strUID As New System.Text.StringBuilder

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine(" CODE_ISO,")
            strSQL.AppendLine(" CODE_ISO_AFFLICATION,")
            strSQL.AppendLine(" CODE_ISO_COUNTRY,")
            strSQL.AppendLine(" CODE_ORGANIZATION,")
            strSQL.AppendLine(" CODE_PRODUCT,")
            strSQL.AppendLine(" CODE_SYSTEM,")
            strSQL.AppendLine(" CODE_HOSPITAL,")
            strSQL.AppendLine(" CODE_SERIES ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine(" MST_STUDYINSTANCEUID")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///取得結果を構造体へ退避
            With typStudyUID
                .CODE_ISO = dsData.Tables(0).Rows(0).Item("CODE_ISO")
                .CODE_ISO_AFFIATION = dsData.Tables(0).Rows(0).Item("CODE_ISO_AFFLICATION")
                .CODE_ISO_COUNTRY = dsData.Tables(0).Rows(0).Item("CODE_ISO_COUNTRY")
                .CODE_ORGANIZATION = dsData.Tables(0).Rows(0).Item("CODE_ORGANIZATION")
                .CODE_PRODUCT = dsData.Tables(0).Rows(0).Item("CODE_PRODUCT")
                .CODE_SYSTEM = dsData.Tables(0).Rows(0).Item("CODE_SYSTEM")
                .CODE_HOSPTAL = dsData.Tables(0).Rows(0).Item("CODE_HOSPITAL")
                .CODE_SERIES = dsData.Tables(0).Rows(0).Item("CODE_SERIES")

                '///サーバ日付/時刻の取得
                '---Ver.1.7.1 Study Instance 重複防止のため変更 UpDated By Watanabe 2022.04.15
                'strWork = fncGetServerDate(2, strMsg)
                strWork = fncGetServerDate(3, strMsg)
                strWorkDate = strWork.Split(",")
                .CODE_SYSDATE = strWorkDate(0).Trim & strWorkDate(1).Trim

                '---Ver.1.7.1  UpDated By Watanabe 2022.04.15
                Erase strWorkDate

                '///UID発行用テーブルのデータを削除
                strSQL.Clear()
                strSQL.AppendLine("DELETE ")
                strSQL.AppendLine("FROM ")
                strSQL.AppendLine(" tran_uid ")
                intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
                '///待機サーバSQL発行
                'If My.Settings.MAIN_SV_DIV = True Then
                '    intRet = fncExecuteNonQuery(objConnect_StandbyDB, strSQL.ToString, strMsg)
                'End If

                '---Ver.1.7.1  UpDated By Watanabe 2022.04.15
                strWork = fncGetServerDate(2, strMsg)
                strWorkDate = strWork.Split(",")

                '///UID発行用テーブルにデータを登録
                strSQL.Clear()
                strSQL.AppendLine("INSERT INTO tran_uid ")
                strSQL.AppendLine("(code_series,")
                strSQL.AppendLine("code_date,")
                strSQL.AppendLine("code_time )")
                strSQL.AppendLine("VALUES (")
                strSQL.AppendLine("'" & .CODE_SERIES & "',")
                strSQL.AppendLine("'" & strWorkDate(0).Trim & "',")
                strSQL.AppendLine("'" & strWorkDate(1).Trim & "')")
                intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
                '///待機サーバSQL発行
                'If My.Settings.MAIN_SV_DIV = True Then
                '    intRet = fncExecuteNonQuery(objConnect_StandbyDB, strSQL.ToString, strMsg)
                'End If

                dsWork.Dispose()

                '///新規UIDを取得
                strSQL.Clear()
                strSQL.AppendLine("SELECT ")
                strSQL.AppendLine(" code_uid_ano ")
                strSQL.AppendLine("FROM ")
                strSQL.AppendLine(" tran_uid ")
                dsWork = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)
                .CODE_SEQ = dsWork.Tables(0).Rows(0).Item("code_uid_ano")

                '///戻り値の編集
                strUID.Clear()
                strUID.Append(.CODE_ISO & ".")
                strUID.Append(.CODE_ISO_AFFIATION & ".")
                strUID.Append(.CODE_ISO_COUNTRY & ".")
                strUID.Append(.CODE_ORGANIZATION & ".")
                strUID.Append(.CODE_PRODUCT & ".")
                strUID.Append(.CODE_HOSPTAL & ".")
                strUID.Append(.CODE_SYSTEM & ".")
                strUID.Append(.CODE_SYSDATE & ".")
                strUID.Append(.CODE_SEQ)

                Return strUID.ToString

            End With
        Catch ex As Exception
            strMsg = ex.Message
            Return ""
        Finally
            dsData.Dispose()
            dsData = Nothing
            dsWork.Dispose()
            dsWork = Nothing
        End Try
    End Function
#End Region

#Region "オーダ情報テーブル更新(subUpdOrder)"
    '*******************************************************************
    '* 機能概要　：オーダ情報テーブル更新
    '* 関数名称　：subUpdOrder
    '* 引　数　　：strMsg　・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subUpdOrder(typData As TYPE_TRAN_ORDER, ByRef strMsg As String)
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer
        Dim dsData As New DataSet
        Dim lngOrderAno As Long

        Try

            With typData
                '///データ存在確認用SQL文生成
                strSQL.Clear()
                strSQL.AppendLine("SELECT ")
                strSQL.AppendLine("ORDER_ANO ")
                strSQL.AppendLine("FROM ")
                strSQL.AppendLine("TRAN_ORDER ")
                strSQL.AppendLine("WHERE ")
                strSQL.AppendLine("ORDER_ANO = " & .ORDER_ANO)

                '///データ存在確認用SQL発行
                dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)
                lngOrderAno = dsData.Tables(0).Rows(0).Item("ORDER_ANO")

                '///データ更新用SQL文
                strSQL.Clear()
                strSQL.AppendLine("UPDATE TRAN_ORDER SET ")
                strSQL.AppendLine("MWM_STATE = '2',")
                strSQL.AppendLine("JISSHI_DATE = '" & Date.Now & "',")
                strSQL.AppendLine("UPD_DATE = '" & Date.Now.ToString("yyyyMMdd") & "',")
                strSQL.AppendLine("UPD_TIME = '" & Date.Now.ToString("HHmmss") & "',")
                strSQL.AppendLine("UPD_USER = 'MAKE_WL' ")
                strSQL.AppendLine("WHERE ")
                strSQL.AppendLine("ORDER_ANO = " & lngOrderAno & " ")

                '///データ更新用SQL発行
                intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
                '///待機サーバSQL発行
                'If My.Settings.MAIN_SV_DIV = True Then
                '    intRet = fncExecuteNonQuery(objConnect_StandbyDB, strSQL.ToString, strMsg)
                'End If
            End With

        Catch ex As Exception
            strMsg = ex.Message

        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub
#End Region

#Region "MWMファイル削除(subDelMWM)"
    '*******************************************************************
    '* 機能概要　：MWMファイル削除
    '* 関数名称　：subDelMWM
    '* 引　数　　：strMsg　・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subDelMWM(ByVal typData As TYPE_TRAN_ORDER)
        Dim aryFiles As New ArrayList
        Dim lngCounter As Long = 0
        Dim strMsg As String = vbNullString

        Call subDispMsg(0, "ファイル移動開始 (" & typData.HOME_PATH & ")", strMsg)
        Call subOutLog("ファイル移動開始 (" & typData.HOME_PATH & ")", 0)

        '*********************************************************************************
        '* MWM作成用フォルダ内の不要ファイルを削除する為、フォルダ内のファイル名を全取得 *
        '*********************************************************************************
        Call subGetAllFiles(typData.HOME_PATH, "*.*", aryFiles, 0)

        'Call subDispMsg(0, "フォルダ内不要ファイル名取得", strMsg)
        'Call subOutLog("フォルダ内不要ファイル名取得", 0)

        'Call subOutLog("移動元 = " & aryFiles(lngCounter).ToString, 0)
        'Call subOutLog("移動先 = " & aryFiles(lngCounter).ToString.Replace(My.Settings.HOME_PATH & My.Settings.TERMINAL, My.Settings.HOME_PATH & My.Settings.TERMINAL & "BK\"), 0)

        '***********************************************
        '* MWM作成用フォルダ内の不要ファイルを移動する *
        '***********************************************
        For lngCounter = 0 To aryFiles.Count - 1
            If InStr(aryFiles(lngCounter).ToString, "lockfile") = 0 Then
                '///ファイル移動前に移動先に同名ファイルが存在するかチェック ADD By Watanabe 2014.02.18
                If System.IO.File.Exists(aryFiles(lngCounter).ToString.Replace(typData.HOME_PATH, typData.HOME_PATH & "\BK\")) = True Then
                    '///移動先に同名ファイルが存在した場合、移動先の同名ファイルを削除する。
                    System.IO.File.Delete(aryFiles(lngCounter).ToString.Replace(typData.HOME_PATH, typData.HOME_PATH & "\BK\"))
                End If
                Threading.Thread.Sleep(1000)

                System.IO.File.Move(aryFiles(lngCounter).ToString,
                                    aryFiles(lngCounter).ToString.Replace(typData.HOME_PATH, typData.HOME_PATH & "\BK\"))
                Call subDispMsg(0, "移動ファイル (" & aryFiles(lngCounter).ToString & ")", strMsg)
                Call subOutLog("移動ファイル (" & aryFiles(lngCounter).ToString & ")", 0)
            End If


        Next lngCounter

    End Sub
#End Region

#Region "オーダ単位でのModality情報を取得"
    ''' <summary>
    ''' オーダ単位でのModality情報を取得
    ''' </summary>
    ''' <param name="objOrder">オーダ情報構造体</param>
    Private Sub subGetModalityInfo(ByRef objOrder As TYPE_TRAN_ORDER)
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim strMsg As String = vbNullString
        Dim intCounter As Integer = 0

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("ISNULL(MODALITY_CD,'') AS MODALITY_CD,")
            strSQL.AppendLine("ISNULL(SCP_AE,'') AS SCP_AE,")
            strSQL.AppendLine("ISNULL(SCU_AE,'') AS SCU_AE,")
            strSQL.AppendLine("ISNULL(SCU_IP,'') AS SCU_IP,")
            strSQL.AppendLine("SCU_PORT,")
            strSQL.AppendLine("ISNULL(HOME_PATH,'') AS HOME_PATH,")
            strSQL.AppendLine("ISNULL(CHARCTERSET,'') AS CHARCTERSET ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_MODALITY ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("TERMINAL = '" & objOrder.TERMINAL & "' ")
            strSQL.AppendLine("AND")
            strSQL.AppendLine("DEL_FLG = 'False'")
            strSQL.AppendLine("ORDER BY MODALITY_NO")


            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///構造体クラスの初期化
            Erase aryModality

            For intCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryModality(intCounter)
                aryModality(intCounter) = New clsMstModality
                '///取得データを構造体クラスへ退避
                With aryModality(intCounter)
                    .MODALITY_NO = dsData.Tables(0).Rows(intCounter).Item("MODALITY_NO")
                    .MODALITY_CD = dsData.Tables(0).Rows(intCounter).Item("MODALITY_CD")
                    .SCP_AE = dsData.Tables(0).Rows(intCounter).Item("SCP_AE")
                    .SCU_AE = dsData.Tables(0).Rows(intCounter).Item("SCU_AE")
                    .SCU_IP = dsData.Tables(0).Rows(intCounter).Item("SCU_IP")
                    .SCU_PORT = dsData.Tables(0).Rows(intCounter).Item("SCU_PORT")
                    .CHARCTERSET = dsData.Tables(0).Rows(intCounter).Item("CHARCTERSET")
                    .HOME_PATH = dsData.Tables(0).Rows(intCounter).Item("HOME_PATH")
                End With
            Next intCounter
        Catch ex As Exception
            strMsg = ex.Message
        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub

#End Region

#End Region

End Class
