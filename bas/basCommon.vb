﻿Imports System.Data.SqlClient
Imports System.Text

Module basCommon

#Region "変数定義"
    Public bolSystem As Boolean
    Public g_SystemStart As Integer
    Public objConnect_LocalDB As New SqlConnection      'SQL Connection
    Public objConnect_StandbyDB As New SqlConnection      'SQL Connection
    'Public typUserInf As New clsUserInf                 'ログインユーザ情報退避用
    Public boNew As Boolean
    Public Clipped As Boolean           'コントロール状態保存フラグ
    Public ctls As Collection           '各コントロール状態の退避エリア
    Public clpScaleWidth As Double      'コントロールの幅
    Public clpScaleHeight As Double     'コントロールの高さ
    Public G_FONT As String = vbNullString
#End Region

#Region "定数定義"
    Public RET_NORMAL As Integer = 0
    Public RET_NOTFOUND As Integer = -1
    Public RET_ERROR As Integer = -9
#End Region

#Region "ログ出力"
    '*********************************************************************
    '* 関数名　　：subOutLog
    '* 関数概要　：所定フォルダログ出力
    '* 引　数　　：strMsg・・・出力メッセージ
    '* 　　　　　：intProc・・・メッセージ区分(0=Information、1=Error、2=Warning)
    '* 戻り値　　：無
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Sub subOutLog(ByVal strMsg As String, ByVal intProc As Integer)

        Dim path_LOG As String = String.Empty
        Dim strPath As String = String.Empty

        If My.Settings.LOG_PATH.EndsWith("\") Then
            path_LOG = My.Settings.LOG_PATH
        Else
            path_LOG = My.Settings.LOG_PATH + "\"
        End If

        '****************************************************
        '* ログ出力所定パス+ 年のフォルダが無ければ作成する *
        '****************************************************
        strPath = path_LOG & DateTime.Now.ToString("yyyy")
        If System.IO.File.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '*********************************************************
        '* ログ出力所定パス+ 年 + 月のフォルダが無ければ作成する *
        '*********************************************************
        strPath &= "\" & DateTime.Now.ToString("MM")
        If System.IO.File.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '**************************************************************
        '* ログ出力所定パス+ 年 + 月 + 日のフォルダが無ければ作成する *
        '**************************************************************
        'strPath &= "\" & DateTime.Now.ToString("dd")
        'If System.IO.File.Exists(strPath) = False Then
        '    System.IO.Directory.CreateDirectory(strPath)
        'End If

        Dim file_LOG As String = String.Empty

        file_LOG = strPath & "\" & _
                   DateTime.Now.ToString("yyyyMMdd") + ".log"

        Dim objsw As New System.IO.StreamWriter(file_LOG, _
                                                True, _
                                                System.Text.Encoding.GetEncoding(932))
        Dim strCate As String = ""

        Select Case intProc
            Case 0          'Information
                strCate = "[INF] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
            Case 1          'Error
                strCate = "[ERR] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
            Case 2          'Warning
                strCate = "[WAR] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
        End Select

        'Console.WriteLine(strCate & strMsg)
        'Console.ReadLine()

        objsw.WriteLine(strCate & strMsg)

        objsw.Close()

    End Sub
#End Region

#Region "SQLサーバConnection"
    Public Function fncDBConnect(ByRef strMsg As String) As Boolean

        'ﾛｸﾞ用ﾃｰﾌﾞﾙ接続
        Dim connectionString As New System.Text.StringBuilder
        Try

            connectionString.Append("Data Source=")
            connectionString.Append(My.Settings.DATASOURCE)
            connectionString.Append(";")
            connectionString.Append("Initial Catalog=")
            connectionString.Append(My.Settings.CATALOG)
            connectionString.Append(";")
            connectionString.Append("UID=")
            connectionString.Append(My.Settings.DBUSER)
            connectionString.Append(";")
            connectionString.Append("PWD=")
            connectionString.Append(My.Settings.DBPASS)
            connectionString.Append(";")
            connectionString.Append("Integrated Security=false;")
            'connectionString.Append("Connect Timeout = 30;")

            'Call subOutLog("データベース接続 開始 (パラメータ = " & connectionString.ToString & ") " & _
            '               "[basRepCommon.fncDBConnect]", 0)

            'objConnect_LocalDB = New SqlConnection
            With objConnect_LocalDB
                .ConnectionString = connectionString.ToString
                .Open()
            End With

            '///待機サーバ接続
            'If My.Settings.MAIN_SV_DIV = True Then
            '    connectionString.Clear()
            '    connectionString.Append("Data Source=")
            '    connectionString.Append(My.Settings.STANDBY_SV)
            '    connectionString.Append(";")
            '    connectionString.Append("Initial Catalog=")
            '    connectionString.Append(My.Settings.CATALOG)
            '    connectionString.Append(";")
            '    connectionString.Append("UID=")
            '    connectionString.Append(My.Settings.DBUSER)
            '    connectionString.Append(";")
            '    connectionString.Append("PWD=")
            '    connectionString.Append(My.Settings.DBPASS)
            '    connectionString.Append(";")
            '    connectionString.Append("Integrated Security=false;")
            '    'connectionString.Append("Connect Timeout = 30;")

            '    'objConnect_StandbyDB = New SqlConnection
            '    With objConnect_StandbyDB
            '        .ConnectionString = connectionString.ToString
            '        .Open()
            '    End With
            'End If
            Return True

        Catch ex As Exception

            strMsg = ex.Message

            Call subOutLog("データベース接続 失敗 (エラー内容 = " & strMsg & ") " & connectionString.ToString & _
                           "[basRepCommon.fncDBConnect]", 1)
            Return False

        End Try

    End Function
#End Region

#Region "SQLサーバDisConnect"
    Public Function fncDBDisConnect(ByRef strMsg As String) As Boolean

        Try

             If Not objConnect_LocalDB Is Nothing Then
                objConnect_LocalDB.Close()
                objConnect_LocalDB.Dispose()
            End If

            '///待機サーバのセッションを切る
            'If Not objConnect_StandbyDB Is Nothing Then
            '    objConnect_StandbyDB.Close()
            '    objConnect_StandbyDB.Dispose()
            'End If

            Return True
        Catch ex As Exception
            strMsg = ex.Message
            Return False
        End Try
    End Function
#End Region

#Region "SQL発行"
    Public Function fncAdptSQL(ByRef objCon As SqlConnection, ByVal strSQL As String, ByRef strMsg As String) As DataSet

        fncAdptSQL = New DataSet

        Try
            Dim objAdpt As SqlDataAdapter = New SqlDataAdapter(strSQL, objCon)

            objAdpt.SelectCommand.CommandTimeout = 0
            objAdpt.Fill(fncAdptSQL)

            objAdpt.Dispose()
            fncAdptSQL.Dispose()

        Catch ex As Exception

            strMsg = ex.Message
            subOutLog("fncAdptSQL Error:" & ex.Message, 1)

        End Try

    End Function


    Public Function fncExecuteNonQuery(ByRef objCon As SqlConnection, ByVal strSQL As String, Optional ByRef strMsg As String = "") As Integer

        Try

            Dim objCommand As SqlCommand

            objCommand = objCon.CreateCommand()
            objCommand.CommandText = strSQL

            Dim iResult As Integer = objCommand.ExecuteNonQuery()
            objCommand.Dispose()

            Return iResult

        Catch ex As Exception

            Debug.WriteLine(ex.Message)
            subOutLog(ex.Message, 1)
            Return 0

        End Try

    End Function

#End Region

#Region "所定フォルダファイル名取得"
    '*********************************************************************
    '* 関数名　　：subGetAllFiles
    '* 関数概要　：所定フォルダのファイル名取得
    '* 引　数　　：strFolder ・・・対象フォルダPath
    '* 　　　　　：strPattern・・・取得ファイルのパターン
    '* 　　　　　：aryFiles　・・・ファイル名退避用
    '* 　　　　　：intProc　 ・・・処理フラグ(0=所定フォルダ内のみ、1=サブフォルダも含める)
    '* 戻り値　　：DataSet
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Sub subGetAllFiles(ByVal strFolder As String, _
        ByVal strPattern As String, ByRef aryFiles As ArrayList, ByVal intProc As Integer)

        '**********************************
        '* Folderにあるファイルを取得する *
        '**********************************
        Dim fs As String() = _
            System.IO.Directory.GetFiles(strFolder, strPattern)

        '***********************
        '* ArrayListに追加する *
        '***********************
        aryFiles.AddRange(fs)

        If intProc = 0 Then Exit Sub

        '**************************
        '* サブフォルダを取得する *
        '**************************
        Dim ds As String() = System.IO.Directory.GetDirectories(strFolder)

        '**************************************
        '* サブフォルダにあるファイルも調べる *
        '**************************************
        Dim d As String
        For Each d In ds
            subGetAllFiles(d, strPattern, aryFiles, 1)
        Next d

    End Sub
#End Region

    '#Region "fncChkLogin　ログイン認証"
    '    '************************************************************
    '    '* 関数名称　：fncChkLogin
    '    '* 機能概要　：ログイン認証
    '    '* 引　数　　：strUserID・・・ユーザID
    '    '* 　　　　　：strPass　・・・パスワード
    '    '* 　　　　　：strMsg　 ・・・エラーメッセージ
    '    '* 戻り値　　：無
    '    '* 作成日付　：2010/0716
    '    '* 作成者　　：Created By Watanabe
    '    '* 更新履歴　：
    '    '************************************************************
    '    Public Function fncChkLogin(ByVal strUserID As String, ByVal strPass As String, ByRef strMsg As String) As Integer

    '        Dim strSQL As New System.Text.StringBuilder
    '        Dim dsUser As New DataSet

    '        Try
    '            '*************
    '            '* SQL文編集 *
    '            '*************
    '            strSQL.Append("SELECT " & vbCrLf)
    '            strSQL.Append(" USER_ANO," & vbCrLf)        'レポートユーザ番号(オートインクリメント)
    '            strSQL.Append(" USER_ID," & vbCrLf)         'レポートユーザID
    '            strSQL.Append(" USER_PASS," & vbCrLf)       'レポートユーザパスワード
    '            strSQL.Append(" USER_NM," & vbCrLf)         'レポートユーザ名
    '            strSQL.Append(" USER_DISP," & vbCrLf)       'レポートユーザ表示名
    '            strSQL.Append(" USER_AUTH, " & vbCrLf)      'レポートユーザ権限(0=システム管理者、1=最終確定者、2=1次確定者、3=参照権限)
    '            strSQL.Append(" DISP_MAINTE," & vbCrLf)     'メンテナンス画面表示フラグ(0=非表示、1=表示)
    '            strSQL.Append(" DISP_MENU " & vbCrLf)       'レポートメニュー画面表示フラグ(0=非表示、1=表示)
    '            strSQL.Append("FROM " & vbCrLf)
    '            strSQL.Append(" MST_USER " & vbCrLf)
    '            strSQL.Append("WHERE " & vbCrLf)
    '            strSQL.Append(" USER_ID = '" & strUserID & "' ")
    '            strSQL.Append("AND " & vbCrLf)
    '            strSQL.Append(" DEL_FLG = '0' ")

    '            '*************
    '            '* SQL文発行 *
    '            '*************
    '            dsUser = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

    '            '*********************************
    '            '* 該当データなしの場合、1を戻す *
    '            '*********************************
    '            If dsUser.Tables.Count = 0 Or dsUser.Tables(0).Rows.Count = 0 Then
    '                Return 1            '該当データなし
    '            End If

    '            '**********************************************************
    '            '* PassWordまで一致した場合は0、パスワード不一致は2を戻す *
    '            '**********************************************************
    '            If strPass.Trim = dsUser.Tables(0).Rows(0).Item("USER_PASS") Then

    '                With typUserInf
    '                    .USER_ANO = dsUser.Tables(0).Rows(0).Item("USER_ANO")
    '                    .USER_ID = dsUser.Tables(0).Rows(0).Item("USER_ID")
    '                    .USER_PASS = dsUser.Tables(0).Rows(0).Item("USER_PASS")
    '                    .USER_NM = dsUser.Tables(0).Rows(0).Item("USER_NM")
    '                    .USER_DISP = dsUser.Tables(0).Rows(0).Item("USER_DISP")
    '                    .USER_AUTH = dsUser.Tables(0).Rows(0).Item("USER_AUTH")
    '                    .DISP_MAINTE = dsUser.Tables(0).Rows(0).Item("DISP_MAINTE")
    '                    .DISP_MENU = dsUser.Tables(0).Rows(0).Item("DISP_MENU")
    '                End With

    '                Return 0            '一致
    '            Else
    '                Return 2            '不一致
    '            End If

    '        Catch ex As Exception

    '            '**************************************************
    '            '* 実行時エラーの場合、エラーメッセージと99を戻す *
    '            '**************************************************
    '            strMsg = ex.Message
    '            Return 99

    '        Finally
    '            dsUser.Dispose()
    '            dsUser = Nothing
    '        End Try
    '    End Function

    '#End Region

#Region "gncGetServerDate サーバ日付取得"
    '************************************************************
    '* 関数名称　：gncGetServerDate
    '* 機能概要　：サーバ日付取得
    '* 引　数　　：intProc・・・0=日付取得
    '* 　　　　　：       　　　1=時刻取得
    '* 　　　　　：             2=日付＋時刻
    '* 　　　　　：strMsg・・・エラーメッセージ
    '* 戻り値　　：String・・・サーバ日付
    '* 　　　　　：      　　　intProcが2の場合 日付 & "," & 時刻
    '* 作成日付　：2010/10/08
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Function fncGetServerDate(ByVal intProc As Integer, ByRef strMsg As String) As String
        Dim strDate As String = vbNullString
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL生成
            strSQL.Clear()

            If intProc = 0 Then
                strSQL.AppendLine("SELECT CONVERT(char(10), GETDATE(), 112) AS NOW_DATE ")
            ElseIf intProc = 1 Then
                strSQL.AppendLine("SELECT REPLACE(CONVERT(varchar,GETDATE(),108),':','') AS NOW_TIME")
            ElseIf intProc = 2 Then
                strSQL.AppendLine("SELECT ")
                strSQL.AppendLine(" CONVERT(char(10), GETDATE(), 112) AS NOW_DATE,")
                strSQL.AppendLine(" REPLACE(CONVERT(varchar,GETDATE(),108),':','') AS NOW_TIME ")
            ElseIf intproc = 3 Then
                '---Ver.1.7.1 Study Instance UIDに付与するため追加 ADD By Watanabe 2022.04.15
                strSQL.AppendLine("SELECT ")
                strSQL.AppendLine(" CONVERT(char(10), GETDATE(), 112) AS NOW_DATE,")
                strSQL.AppendLine(" REPLACE(CONVERT(varchar,GETDATE(),114),':','') AS NOW_TIME ")
            End If

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If intProc = 0 Then
                '///取得データを退避
                strDate = dsData.Tables(0).Rows(0).Item("NOW_DATE")
            ElseIf intProc = 1 Then
                '///取得データを退避
                strDate = dsData.Tables(0).Rows(0).Item("NOW_TIME")
            ElseIf intProc = 2 Then
                strDate = dsData.Tables(0).Rows(0).Item("NOW_DATE")
                strDate &= "," & dsData.Tables(0).Rows(0).Item("NOW_TIME")
            ElseIf intProc = 3 Then
                '---Ver.1.7.1 Study Instance UIDに付与するため追加 ADD By Watanabe 2022.04.15
                strDate = dsData.Tables(0).Rows(0).Item("NOW_DATE")
                strDate &= "," & dsData.Tables(0).Rows(0).Item("NOW_TIME")
            End If

            Return strDate

        Catch ex As Exception
            strMsg = ex.Message
            Return vbNullString
        End Try
    End Function
#End Region

#Region "fncGetStringLength 文字列のバイト数取得"
    '************************************************************
    '* 関数名称　：fncGetStringLength
    '* 機能概要　：文字列のバイト数取得
    '* 引　数　　：strText・・・String
    '* 戻り値　　：Integer・・・渡された文字列のバイト数を戻す  
    '* 作成日付　：2010/07/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Function fncGetStringLength(ByVal strText As String) As Integer
        Dim strStr As String
        Dim sjisEnc As Encoding
        Dim intStrByte As Integer

        strStr = strText
        sjisEnc = Encoding.GetEncoding("Shift_JIS")

        intStrByte = sjisEnc.GetByteCount(strStr)

        Return intStrByte
    End Function
#End Region

#Region "subHightLight テキストボックスのハイライト表示"
    '************************************************************
    '* 関数名称　：subHightLight
    '* 機能概要　：テキストボックスのハイライト表示
    '* 引　数　　：objText・・・TextBox
    '* 戻り値　　：無  
    '* 作成日付　：2010/07/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Sub subHightLight(ByRef objText As Object)
        objText.SelectionStart = 0
        objText.SelectionLength = objText.TextLength
    End Sub
#End Region

#Region "所定フォルダへファイル移動(subMoveWorkFolder)"
    '*********************************************************************
    '* 関数名　　：subMoveWorkFolder
    '* 関数概要　：所定フォルダへファイル移動
    '* 引　数　　：strMsg・・・出力メッセージ
    '* 　　　　　：intProc・・・メッセージ区分(0=Information、1=Error、2=Warning)
    '* 戻り値　　：無
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Sub subMoveWorkFolder(typData As clsOrderInf, ByRef strFileName As String, ByVal strMsg As String, ByVal intProc As Integer)

        Dim path_LOG As String = String.Empty
        Dim strPath As String = String.Empty

        If typData.HOME_PATH.EndsWith("\") Then
            path_LOG = typData.HOME_PATH
        Else
            path_LOG = typData.HOME_PATH + "\"
        End If

        If intProc = 0 Then
            path_LOG &= "OK\"
        Else
            path_LOG &= "ERR\"
        End If

        '********************************************************
        '* ファイル出力所定パス+ 年のフォルダが無ければ作成する *
        '********************************************************
        strPath = path_LOG & DateTime.Now.ToString("yyyy")
        If System.IO.File.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '*************************************************************
        '* ファイル出力所定パス+ 年 + 月のフォルダが無ければ作成する *
        '*************************************************************
        strPath &= "\" & DateTime.Now.ToString("MM")
        If System.IO.File.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '******************************************************************
        '* ファイル出力所定パス+ 年 + 月 + 日のフォルダが無ければ作成する *
        '******************************************************************
        strPath &= "\" & DateTime.Now.ToString("dd")
        If System.IO.File.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If
        strPath &= "\"
        System.IO.File.Move(typData.HOME_PATH & strFileName, strPath & strFileName)

    End Sub
#End Region

    Public Function LenB(ByVal str As String) As Integer
        'Shift JISに変換したときに必要なバイト数を返す
        Return System.Text.Encoding.GetEncoding(932).GetByteCount(str)
    End Function

End Module
