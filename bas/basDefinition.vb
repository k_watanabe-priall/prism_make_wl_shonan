﻿Imports System.Data.SqlClient

Module basDefinition

#Region "定数定義"
    Public Const G_CANCEL As String = "Pingをキャンセルします。"
    Public Const G_QUESTION As String = "宜しいですか。"
#End Region

#Region "変数定義"
    'Public objConnect_LocalDB As New SqlConnection      'SQL Connection

    '    Public G_FONT As String = vbNullString

    Public RET_NORMAL As Integer = 0                    '関数戻り値　正常 = 0
    Public RET_NOTFOUND As Integer = -1                 '関数戻り値　データ無 = -1
    Public RET_ERROR As Integer = -9                    '関数戻り値　エラー = -9
    Public RET_WARNING As Integer = -2                  '関数戻り値　ワーニング = -2

    Public typUserInf As New clsUser                    'ログインユーザ情報退避用
    Public g_SystemStart As Integer

#End Region

End Module
